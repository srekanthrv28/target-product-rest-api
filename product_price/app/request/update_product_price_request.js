/* @flow */
'use strict';

export interface UpdateProductPriceRequest {
  +product_id: string,
  +value: number,
  +currency_code: string
}

export const InitialUpdateProductPriceRequest = {
  product_id: '',
  value: '',
  currency_code: ''
};

export const UpdateProductPriceRequestConstraints = {
  product_id: {
    presence: true,
    length: {
      minimum: 5,
      maximum: 512
    }
  },
  value: {
    presence: true
  },
  currency_code: {
    presence: true,
    length: {
      maximum: 1024
    }
  }
};
