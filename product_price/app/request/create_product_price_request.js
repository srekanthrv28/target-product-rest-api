/* @flow */
'use strict';

export interface CreateProductPriceRequest {
  +product_id: string,
  +value: number,
  +currency_code: string
}

export const InitialCreateProductPriceRequest = {
  product_id: '',
  value: '',
  currency_code: ''
};

export const CreateProductPriceRequestConstraints = {
  product_id: {
    presence: true,
    length: {
      minimum: 5,
      maximum: 512
    }
  },
  value: {
    presence: true
  },
  currency_code: {
    length: {
      maximum: 1024
    }
  }
};
