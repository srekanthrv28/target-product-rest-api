/* @flow */
'use strict';

import {
  CreateProductPriceRequest,
  CreateProductPriceRequestConstraints
} from '../request/create_product_price_request';

import {
  GetProductPriceRequest,
  InitialGetProductPriceRequestConstraints
} from '../request/get_product_price_request';

import {
  UpdateProductPriceRequest,
  UpdateProductPriceRequestConstraints
} from '../request/update_product_price_request';

import * as validate from 'validate.js';
import {
  InitialAppError
} from 'tiny_error';
import R from 'ramda'

export function validate_request_async(create_product_price_request: CreateProductPriceRequest): Promise<>  {
  const validation_result: mixed = validate.validate(create_product_price_request, CreateProductPriceRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_product_price_request);
  }
  return send_error(validation_result)
}

export function validate_get_request_async(get_product_price_request: GetProductPriceRequest): Promise<> {
  const validation_result: mixed = validate.validate(get_product_price_request, InitialGetProductPriceRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(get_product_price_request);
  }
  return send_error(validation_result)
}

export function validate_update_request_async(update_product_price_request: UpdateProductPriceRequest): Promise<> {
  const validation_result: mixed = validate.validate(update_product_price_request, UpdateProductPriceRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(update_product_price_request);
  }
  return send_error(validation_result)
}

function send_error(validation_result: mixed): Promise<>  {
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}
