/* @flow */
'use strict';

export interface GetProductPriceRequest {
  +product_id: string
}

export const InitialGetProductPriceRequest: GetProductPriceRequest = {
  product_id: ''
};

export const InitialGetProductPriceRequestConstraints  = {
  product_id: {
    presence: true
  }
}
