/* @flow */
'use strict';

import * as create_product_price_interactor from '../interactor/create_product_price_interactor';
import * as get_product_price_interactor from '../interactor/get_product_price_interactor';
import * as update_product_price_interactor from '../interactor/update_product_price_interactor';
import { CreateProductPriceRequest } from '../request/create_product_price_request';
import { GetProductPriceRequest } from '../request/get_product_price_request';
import { UpdateProductPriceRequest } from '../request/update_product_price_request';
import {ProductPriceDatastoreProvider} from '../datastore/product_price_datastore_provider';

export function create_product_price(datastore: ProductPriceDatastoreProvider, create_product_price_request: CreateProductPriceRequest): Promise<*> {
  return create_product_price_interactor.create_or_update_datastore_product_price(datastore, create_product_price_request);
}

export function get_product_price(datastore: ProductPriceDatastoreProvider, get_product_price_request: GetProductPriceRequest): Promise<*> {
  return get_product_price_interactor.get_product_price(datastore, get_product_price_request);
}

export function update_product_price(datastore: ProductPriceDatastoreProvider, update_product_price_request: UpdateProductPriceRequest): Promise<*> {
  return update_product_price_interactor.update_product_price(datastore, update_product_price_request);
}
