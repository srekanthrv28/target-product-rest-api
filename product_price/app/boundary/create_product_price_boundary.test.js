/* @flow */
'use strict';

import {it} from 'mocha';
import * as product_price_boundary from './product_price_boundary';
import {CreateProductPriceRequest} from '../request/create_product_price_request';
import * as in_memory_datastore from '../datastore/in_memory_product_price_datastore';
//import * as faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import uuid_v4 from 'uuid/v4';

const get_create_product_price_request = (): CreateProductPriceRequest => {
  return {
    product_id: uuid_v4(),
    currency_code: 'USD',
    value: 4.23
  }
};

const _create_new_product_price = it("Should create product price with valid create product price request", (): Promise<> => {
  const test_create_request: CreateProductPriceRequest = get_create_product_price_request();
  const create_product_price_promise: Promise<> = product_price_boundary.create_product_price(in_memory_datastore, test_create_request);
  return Promise.all([
    create_product_price_promise.should.eventually.be.fulfilled,
    create_product_price_promise.should.eventually.have.property('product_id'),
    create_product_price_promise.should.eventually.have.property('currency_code'),
  ]);
});

const _dont_create_product_price = it("Should not create product price with out address1", (): Promise<> => {
  const test_create_request: CreateProductPriceRequest = get_create_product_price_request();
  const test_create_invalid_request = R.assoc('address1', '', test_create_request);
  const create_product_price_promise: Promise<> = product_price_boundary.create_product_price(in_memory_datastore, test_create_invalid_request);
  return Promise.all([
    create_product_price_promise.should.eventually.be.rejected
  ]);
});
