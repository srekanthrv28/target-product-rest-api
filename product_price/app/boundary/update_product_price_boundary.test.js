/* @flow */
'use strict';

import {it} from 'mocha';
import * as product_price_boundary from './product_price_boundary';
import {CreateProductPriceRequest} from '../request/create_product_price_request';
import {GetProductPriceRequest} from '../request/get_product_price_request';
import * as in_memory_datastore from '../datastore/in_memory_product_price_datastore';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import uuid_v4 from 'uuid/v4';

const get_create_product_price_request = (): CreateProductPriceRequest => {
  return {
    product_id: uuid_v4(),
    value: 4.5,
    currency_code: 'USD'
  }
};

const get_product_price_request_for_get_request = (): GetProductPriceRequest => {
  return {
    product_id: uuid_v4(),
    value: 2.0,
    currency_code: 'USD'
  }
};

 const _get_product_price = it('Should update product price with valid update product price request', (): Promise<> => {
    const test_create_request: CreateProductPriceRequest = get_create_product_price_request();
    const _create_user_certification = product_price_boundary.create_product_price(in_memory_datastore, test_create_request);
    const test_get_request: GetProductPriceRequest = get_product_price_request_for_get_request(test_create_request);
    const update_product_price_promise = product_price_boundary.update_product_price(in_memory_datastore, test_get_request);
    return Promise.all([
      update_product_price_promise.should.eventually.be.fulfilled,
      update_product_price_promise.should.eventually.have.property('product_id'),
      update_product_price_promise.should.eventually.have.property('value'),
      update_product_price_promise.should.eventually.have.property('currency_code')
    ]);
});
