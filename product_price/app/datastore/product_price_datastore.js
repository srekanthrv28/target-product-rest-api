/* @flow */
'use strict';

import {ProductPriceEntity} from '../entity/product_price_entity';
import * as in_memory_datastore from './in_memory_product_price_datastore';
import {ProductPriceDatastoreProvider} from './product_price_datastore_provider';
import R from 'ramda';


const default_datastore: ProductPriceDatastoreProvider = in_memory_datastore;

export function create_or_update_datastore_product_price(datastore: ProductPriceDatastoreProvider, product_price_entity: ProductPriceEntity): Promise<*> {
  return get_datastore(datastore).create_or_update_datastore_product_price(product_price_entity);
}

export function get_datastore_product_price(datastore: ProductPriceDatastoreProvider, product_price_entity: ProductPriceEntity): Promise<*> {
  return  get_datastore(datastore).get_datastore_product_price(product_price_entity);
}

export function update_datastore_product_price(datastore: ProductPriceDatastoreProvider, product_price_entity: ProductPriceEntity): Promise<*> {
  return get_datastore(datastore).update_datastore_product_price(product_price_entity);
}


function get_datastore(provided_datastore: ProductPriceDatastoreProvider): ProductPriceDatastoreProvider {
  return R.cond([
    [R.isNil, R.always(default_datastore)],
    [R.isEmpty, R.always(default_datastore)],
    [R.T, R.always(provided_datastore)]
  ])(provided_datastore);
}
