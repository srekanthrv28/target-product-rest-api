/* @flow */
'use strict';

import {ProductPriceEntity} from '../entity/product_price_entity';
import * as datastore from 'memory-cache';
import R from 'ramda';
import {InitialAppError} from 'tiny_error';

export function create_or_update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<> {
  return R.ifElse(R.curry(does_product_price_already_exist), update_product_price, create_new_datastore_product_price)(product_price_entity);
}

export function get_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<> {
  return R.ifElse(R.curry(does_product_price_already_exist), get_product_price_from_db, send_empty_response)(product_price_entity);
}

function get_product_price_from_db(product_price_entity: ProductPriceEntity): ProductPriceEntity {
  return datastore.get(product_price_entity.product_id);
}

function does_product_price_already_exist(product_price_entity: ProductPriceEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(product_price_entity.product_id);
}

function error_user_doesnt_exist(product_price_entity: ProductPriceEntity): Promise<Error> {
  return Promise.reject(R.merge(InitialAppError, {reason: `Product with product_id: ", ${product_price_entity.product_id}," doesn't exist...`, type: "INVALID_REQUEST"}));
}

function send_empty_response(): Promise<> {
  return Promise.resolve({});
}

function create_new_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<> {
  return Promise.resolve(datastore.put(product_price_entity.product_id, product_price_entity));
}

export function update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<> {
  return R.ifElse(R.curry(does_product_price_already_exist), update_product_price, error_user_doesnt_exist)(product_price_entity);
}

function update_product_price(product_price_entity: ProductPriceEntity): Promise<> {
  const existing_product_price = datastore.get(product_price_entity.product_id);
  const updated_product_price = R.merge(existing_product_price, product_price_entity);
  return Promise.resolve(datastore.put(product_price_entity.product_id, updated_product_price));
}
