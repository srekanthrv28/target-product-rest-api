/* @flow */
'use strict';

import {ProductPriceEntity} from '../entity/product_price_entity';

export interface ProductPriceDatastoreProvider {
  create_or_update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<>,
  get_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<>,
  update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<>
}
