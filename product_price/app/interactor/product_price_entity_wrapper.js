/* @flow */
'use strict';

import {
  CreateProductPriceRequest
} from '../request/create_product_price_request';
import {
  InitialProductPriceResponse,
  type ProductPriceResponse
} from '../response/product_price_response';
import {
  InitialProductPriceEntity,
  type ProductPriceEntity
} from '../entity/product_price_entity';
import R from 'ramda';

export function from_product_price_entity_to_response(product_price_entity: ProductPriceEntity): ProductPriceResponse {
  if (product_price_entity) {
    return R.pick(R.keys(InitialProductPriceResponse), product_price_entity);
  }
  return InitialProductPriceResponse;
}

export function from_product_price_request_to_entity(request: CreateProductPriceRequest): ProductPriceEntity {
  return R.pick(R.keys(InitialProductPriceEntity), request);
}
