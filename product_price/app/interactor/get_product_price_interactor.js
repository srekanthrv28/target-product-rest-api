/* @flow */
'use strict';

import {logger} from 'app_log';

import {ProductPriceEntity} from '../entity/product_price_entity';
import {GetProductPriceRequest} from '../request/get_product_price_request';
import {ProductPriceResponse} from '../response/product_price_response';
import {ProductPriceDatastoreProvider} from '../datastore/product_price_datastore_provider';

import * as product_price_datastore from "../datastore/product_price_datastore";
import * as request_validator from '../request/product_price_request_validator';
import * as product_price_entity_wrapper from './product_price_entity_wrapper';
import R from "ramda"

export function get_product_price(datastore: ProductPriceDatastoreProvider, request: GetProductPriceRequest): Promise<> {
  const _x = logger.debug(`got the request to Get the product price with request: ${R.toString(request)}`);
  return request_validator.validate_get_request_async(request)
    .then(product_price_entity_wrapper.from_product_price_request_to_entity)
    .then(R.curry(product_price_datastore.get_datastore_product_price)(datastore))
    .then(product_price_fetch_success);
}

function product_price_fetch_success(product_price_entity: ProductPriceEntity): ProductPriceResponse {
  const _x = logger.debug(`successfully getting the product price details: ${R.toString(product_price_entity)}`);
  return product_price_entity_wrapper.from_product_price_entity_to_response(product_price_entity);
}
