/* @flow */
'use strict';

import {logger} from 'app_log';
import R from 'ramda';

import {type UpdateProductPriceRequest} from '../request/update_product_price_request';
import {type ProductPriceEntity} from '../entity/product_price_entity';
import {type ProductPriceResponse} from '../response/product_price_response';
import {ProductPriceDatastoreProvider} from '../datastore/product_price_datastore_provider';
import * as request_validator from '../request/product_price_request_validator';
import * as product_price_datastore from "../datastore/product_price_datastore";
import * as product_price_entity_wrapper from './product_price_entity_wrapper';

export function update_product_price(datastore: ProductPriceDatastoreProvider, request: UpdateProductPriceRequest): Promise<> {
  const _x = logger.info(`got the request to update the product price with request: ${R.toString(request)}`);
  return request_validator.validate_update_request_async(request)
    .then(product_price_entity_wrapper.from_product_price_request_to_entity)
    .then(R.curry(product_price_datastore.update_datastore_product_price)(datastore))
    .then(product_price_fetch_success);
}


function product_price_fetch_success(product_price_entity: ProductPriceEntity): ProductPriceResponse {
  const _y = logger.info(`successfully updated the product: ${R.toString(product_price_entity)}`);
  return product_price_entity_wrapper.from_product_price_entity_to_response(product_price_entity);
}
