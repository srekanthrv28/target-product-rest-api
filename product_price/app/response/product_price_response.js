/* @flow */
'use strict';

export interface ProductPriceResponse {
  +product_id: string,
  +value: number,
  +currency_code: string
}

export const InitialProductPriceResponse: ProductPriceResponse = {
  product_id: '',
  value: '',
  currency_code: ''
};
