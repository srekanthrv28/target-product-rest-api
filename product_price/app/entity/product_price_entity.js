/* @flow */
'use strict';

export interface ProductPriceEntity {
  +product_id: string,
  +value: number,
  +currency_code: string
}

export const InitialProductPriceEntity: ProductPriceEntity = {
  product_id: '',
  value: '',
  currency_code: ''
};
