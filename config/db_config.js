/* @flow */
'use strict';

module.exports = {
  development: {
    "database": "target_rest_db",
    "username": "target_rest",
    "password": "target@123#",
    "host": '127.0.0.1',
    "dialect": 'mysql',
    "migrationStorageTableName": "sequelize_migrations",
    "seederStorageTableName": "sequelize_seeds",
    "seederStorage": "sequelize"
  }
};
