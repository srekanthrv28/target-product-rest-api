/* @flow */
'use strict';

import bunyan from 'bunyan';
import bunyan_format from 'bunyan-format';
import config from 'config';


const node_env = process.env.NODE_ENV == null || process.env.NODE_ENV !== process.env.NODE_ENV ? "development" : process.env.NODE_ENV;
const log_config = config.get(node_env + '.' + 'LogConfig');

const format_out = bunyan_format({color: log_config.ansi_color, outputMode: log_config.output_mode}) // outputMode: short|long|simple|json|bunyan
export const logger = bunyan.createLogger(
                        {
                        name: log_config.app_name,
                        src: log_config.show_source,
                        stream: format_out,
                        level: log_config.log_level
                      });


