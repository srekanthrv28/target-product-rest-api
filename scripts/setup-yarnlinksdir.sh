#!/bin/bash

echo setting yarn link directory to ${1}/.yarnlinks
echo creating ${1}/.yarnrc...
echo "--link-folder "${1}/.yarnlinks"" > ${1}/.yarnrc
