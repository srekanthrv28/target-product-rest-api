How To Run code
* Download and install node version v8.10.0
* Install YARN the node package manager
* Download and install mysql version 5.7 and above
* Clone the project https://gitlab.com/srekanthrv28/target-product-rest-api.git
* Run the below DB script inside the project to create Database
mysql -u root -p < scripts/create_db_setup.sql
* Run yarn predev inside the project.
* Go to directory "db" in inside the project folder and Run the following migration commands.
cd db
yarn sequelize db:migrate
yarn sequelize db:seed:all
* Come back to main project folder.
* Run yarn dev inside the project
* Server will be running on http://127.0.0.1:3000/


Clean Code
* Functional Programming(using Rmadajs)
    * Immutable
* Tests(Unit and Integration Test)
* Static type checkers (flow).

Brief Overview

* It is majorly based on the concepts introduced by Robert C. Martin
* My target is to write modular, and maintainable code
* I tried to follow the clean code principles suggested by Robert C. Martin
* App is divided into multiple modules
* Each module is a separate NPM module
* Each module has own unit tests
* Webapp Module has the integration tests
* Each module in the app follows Entity-Boundary-Interactor architecture
