/* @flow */
'use strict';

import {logger} from 'app_log';
import {CreateProductRequest} from '../request/create_product_request';
import {ProductResponse} from '../response/product_response';
import {create_datastore_product,} from '../datastore/product_datastore';
import {ProductEntity} from '../entity/product_entity';
import * as request_validator from '../request/product_request_validator';
import * as product_entity_wrapper from './product_entity_wrapper';
import {type ProductDatastoreProvider} from '../datastore/product_datastore_provider';
import R from 'ramda';

export function create_product(datastore: ProductDatastoreProvider, request: CreateProductRequest): Promise<*> {
  const _x = logger.info(`Creating product with request: ${R.toString(request)}`);
  return request_validator.validate_create_product_request_async(request)
    .then(product_entity_wrapper.from_product_request_to_entity)
    .then(R.curry(create_datastore_product)(datastore))
    .then(product_creation_success);
}

function product_creation_success(product_entity: ProductEntity): ProductResponse {
  const _x = logger.info(`Successfully created product: ${JSON.stringify(product_entity)}`);
  return product_entity_wrapper.from_product_entity_to_response(product_entity);
}
