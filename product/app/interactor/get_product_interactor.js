/* @flow */
'use strict';

import {logger} from 'app_log';
import {ProductResponse} from '../response/product_response';
import * as product_datastore from '../datastore/product_datastore';
import {type ProductDatastoreProvider} from '../datastore/product_datastore_provider';
import {GetProductRequest} from '../request/get_product_request';
import * as product_entity_wrapper from './product_entity_wrapper';


export async function get_product_from_db(datastore: ProductDatastoreProvider, request: GetProductRequest): Promise<ProductResponse> {
  const product_details = await product_datastore.get_product_from_db(datastore, request);
  return get_product_success(product_details);
}

function get_product_success(product_entity: ProductResponse): ProductResponse {
  const _x = logger.info(`Successfully got the product details: ${JSON.stringify(product_entity)}`);
  return product_entity_wrapper.from_product_entity_to_response(product_entity);
}
