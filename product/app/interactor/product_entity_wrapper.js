/* @flow */
'use strict';

import {CreateProductRequest} from '../request/create_product_request';
import {InitialProductResponse, type ProductResponse} from '../response/product_response';
import {InitialProductEntity, type ProductEntity} from '../entity/product_entity';
import R from 'ramda';

export function from_product_entity_to_response(product_entity: ProductEntity): ProductResponse {
  const response_with_out_id = R.pick(R.keys(InitialProductResponse), product_entity);
  return R.merge(response_with_out_id, {product_id: product_entity.id});
}

export function from_product_request_to_entity(request: CreateProductRequest): ProductEntity {
  return R.pick(R.keys(InitialProductEntity), request);
}
