/* @flow */
'use strict';

import {logger} from 'app_log';
import {UpdateProductRequest} from '../request/update_product_request';
import {ProductResponse} from '../response/product_response';
import {update_datastore_product} from '../datastore/product_datastore';
import {ProductEntity} from '../entity/product_entity';
import * as request_validator from '../request/product_request_validator';
import * as product_entity_wrapper from './product_entity_wrapper';
import {type ProductDatastoreProvider} from '../datastore/product_datastore_provider';
import R from 'ramda';

export function update_product(datastore: ProductDatastoreProvider, request: UpdateProductRequest): Promise<*> {
  const _x = logger.info(`Updating product with request: ${R.toString(request)}`);
  return request_validator.validate_update_product_request_async(request)
    .then(R.curry(update_datastore_product)(datastore))
    .then(product_updation_success);
}

function product_updation_success(product_entity: ProductEntity): ProductResponse {
  const _x = logger.info(`Successfully updated product status: ${JSON.stringify(product_entity)}`);
  return product_entity_wrapper.from_product_entity_to_response(product_entity);
}
