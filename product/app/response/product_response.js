/* @flow */
'use strict';

export interface ProductResponse {
  +name: string,
  +description: string,
  +product_id: string
}

export const InitialProductResponse: ProductResponse = {
  name: '',
  description: '',
  product_id: ''
};
