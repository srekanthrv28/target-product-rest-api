/* @flow */
'use strict';

export interface GetProductRequest {
  +product_id: string
}

export const InitialGetProductRequest: GetProductRequest = {
  product_id: ''
};

export const GetProductRequestConstraints  = {
  product_id: {
    presence: true,
    length: {
      maximum: 512
    }
  }
}
