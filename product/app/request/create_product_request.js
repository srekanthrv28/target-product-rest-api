/* @flow */
'use strict';

export interface CreateProductRequest {
  +name: string,
  +description: string
}

export const InitialCreateProductRequest: CreateProductRequest = {
  name: '',
  description: ''
};

export const CreateProductRequestConstraints  = {
  name: {
    presence: true
  },
  description: {
    presence: true
  }
}
