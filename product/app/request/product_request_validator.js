/* @flow */
'use strict';

import {CreateProductRequest, CreateProductRequestConstraints} from '../request/create_product_request';
import {UpdateProductRequest, UpdateProductRequestConstraints} from '../request/update_product_request';
import * as validate from 'validate.js';
import {InitialAppError} from 'tiny_error';
import R from 'ramda';

export function validate_create_product_request_async(create_product_request: CreateProductRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_product_request, CreateProductRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_product_request);
  }
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}


export function validate_update_product_request_async(product_request: UpdateProductRequest): Promise<*> {
  const validation_result: mixed = validate.validate(product_request, UpdateProductRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(product_request);
  }
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}
