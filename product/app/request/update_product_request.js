/* @flow */
'use strict';

export interface UpdateProductRequest {
  +product_id: string,
  +status: string
}

export const InitialUpdateProductRequest: UpdateProductRequest = {
  product_id: '',
  status: ''
};

export const UpdateProductRequestConstraints  = {
  product_id: {
    presence: true
  }
}
