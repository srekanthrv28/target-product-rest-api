/* @flow */
'use strict';

import {
  ProductEntity
} from '../entity/product_entity';
import datastore from 'memory-cache';
import uuid_v4 from 'uuid/v4';
import R from 'ramda';
import {logger} from 'app_log';
import {InitialAppError} from 'tiny_error';

export function create_datastore_product(product_entity: ProductEntity): Promise<ProductEntity | Error> {
  return R.ifElse(R.curry(does_product_already_exist), error_product_already_exist, create_new_datastore_product)(product_entity);
}

function does_product_already_exist(product_entity: ProductEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(product_entity.cname);
}

function does_product_already_exist_by_id(product_entity: ProductEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(product_entity.id);
}

function create_new_datastore_product(product_entity: ProductEntity): Promise<ProductEntity> {
  const _a = logger.info("Creating new product with data: ", product_entity);
  const new_product_entity: ProductEntity = R.merge(product_entity, {id: uuid_v4()});

  const is_true = (test: *): boolean => R.isNil(test)? R.T : R.F;
  const _product_list = R.ifElse(is_true(datastore.get('product_list')), datastore.put('product_list', R.append(new_product_entity, [])), datastore.put('product_list',R.append(new_product_entity, datastore.get('product_list'))));

  const _add_product_by_id = datastore.put(new_product_entity.id, new_product_entity);
  return Promise.resolve(datastore.put(new_product_entity.cname, new_product_entity));
}

function error_product_already_exist(product_entity: ProductEntity): Promise<Error> {
  const _a = logger.error(`Product with cname ${R.toString(product_entity.cname)} already exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `Product with cname: ", ${product_entity.cname} already exists`, type: "INVALID_REQUEST"}));
}

export function get_product_from_db(product_entity: ProductEntity): Promise<ProductEntity | Error> {
  return R.ifElse(R.curry(does_product_already_exist_by_id), get_product_data, error_product_does_not_exist)(product_entity);
}

export function get_all_agencies_from_db(): Promise<*> {
  return datastore.get('product_list');
}

function get_product_data(product_entity: ProductEntity): Promise<ProductEntity | Error> {
  return datastore.get(product_entity.id);
}

function error_product_does_not_exist(product_entity: ProductEntity): Promise<Error> {
  const _a = logger.error(`product with cname ${R.toString(product_entity.cname)} does not exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `product with email: ", ${product_entity.cname} does not exists`, type: "INVALID_REQUEST"}));
}

export function update_datastore_product(product_entity: ProductEntity): Promise<ProductEntity | Error> {
  return R.ifElse(R.curry(does_product_already_exist),update_required_datastore_product , error_product_does_not_exist )(product_entity);
}

function update_required_datastore_product(product_entity: ProductEntity): Promise<ProductEntity> {
  const _a = logger.info("Updating product with data: ", product_entity);
  const new_product_entity = R.merge(datastore.get(product_entity.id), product_entity);
  const _update_product_id = datastore.put(new_product_entity.id, new_product_entity);
  return Promise.resolve(datastore.put(new_product_entity.cname, new_product_entity));
}
