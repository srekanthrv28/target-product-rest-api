/* @flow */
'use strict';

import {ProductEntity} from '../entity/product_entity';
import {type ProductDatastoreProvider} from './product_datastore_provider';
import * as in_memory_datastore from './in_memory_product_datastore';
import {GetProductRequest} from '../request/get_product_request'
import R from 'ramda';
const default_datastore: ProductDatastoreProvider = in_memory_datastore;

export function create_datastore_product(datastore: ProductDatastoreProvider, product_entity: ProductEntity): Promise<*> {
  return get_datastore(datastore).create_datastore_product(product_entity);
}

export function update_datastore_product(datastore: ProductDatastoreProvider, product_entity: ProductEntity): Promise<*> {
  return get_datastore(datastore).update_datastore_product(product_entity);
}

export function get_product_from_db(datastore: ProductDatastoreProvider, request: GetProductRequest): Promise<*> {
  return get_datastore(datastore).get_product_from_db(request);
}

function get_datastore(provided_datastore: ProductDatastoreProvider): ProductDatastoreProvider {
  return R.cond([
    [R.isNil, R.always(default_datastore)],
    [R.isEmpty, R.always(default_datastore)],
    [R.T, R.always(provided_datastore)]
  ])(provided_datastore);
}
