/* @flow */
'use strict';

import {ProductEntity} from '../entity/product_entity';
import {GetProductRequest} from '../request/get_product_request';

export interface ProductDatastoreProvider {
  create_datastore_product(product_entity: ProductEntity): Promise<*>,
  update_datastore_product(product_entity: ProductEntity): Promise<*>,
  get_product_from_db(request: GetProductRequest): Promise<*>
}
