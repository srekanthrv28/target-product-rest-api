/* @flow */
'use strict';

import {it} from 'mocha';
import * as product_boundary from './product_boundary';
import {CreateProductRequest} from '../request/create_product_request';
import * as faker from 'faker';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import {logger} from 'app_log';
import * as in_memory_datastore from '../datastore/in_memory_product_datastore';
import R from 'ramda';

const get_create_product_request = (): CreateproductRequest => {
  return {
    name: faker.name.firstName() + faker.name.lastName(),
    description: R.toLower(faker.name.firstName())
  }
};

const _create_new_product = it("Should create product with valid create product request", (): Promise<> => {
  const test_create_request: CreateProductRequest = get_create_product_request();
  const create_product_promise: Promise<> = product_boundary.create_new_product(in_memory_datastore, test_create_request);
  return Promise.all([
    create_product_promise.should.eventually.be.fulfilled,
    create_product_promise.should.eventually.have.property('name'),
    create_product_promise.should.eventually.have.property('description')
  ]);
});

const _dont_create_duplicate_product = it("Should not create product with duplicate product id", (): Promise<> => {
  const test_create_request: CreateProductRequest = get_create_product_request();
  const create_product_promise: Promise<> = product_boundary.create_new_product(in_memory_datastore, test_create_request);
  const create_dup_product_promise = product_boundary.create_new_product(in_memory_datastore, test_create_request);
  const _a = logger.info("_dont_create_duplicate_product: ", create_dup_product_promise);
  return Promise.all([
    create_product_promise.should.eventually.be.fulfilled,
    create_product_promise.should.eventually.have.property('name'),
    create_product_promise.should.eventually.have.property('description'),
    create_dup_product_promise.should.eventually.be.rejected
  ]);
});
