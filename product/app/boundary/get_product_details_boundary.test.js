/* @flow */
'use strict';

import {it} from 'mocha';
import * as product_boundary from './product_boundary';
import {CreateProductRequest} from '../request/create_product_request';
import {GetProductRequest} from '../request/get_product_request';
import {ProductResponse} from '../response/product_response';
import * as faker from 'faker';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import * as in_memory_datastore from '../datastore/in_memory_product_datastore';

const get_create_product_request = (): CreateProductRequest => {
  return {
    name: faker.name.firstName() + faker.name.lastName(),
    description: R.toLower(faker.name.firstName())
  }
};

const _get_Product_by_id_test = it("Should create Product and get Product by id", (): Promise<> => {
  const test_create_request: CreateProductRequest = get_create_product_request();
  const create_product_promise: Promise<> = product_boundary.create_new_product(in_memory_datastore, test_create_request);
  const get_Product_promise = create_product_promise.then( (product_response: ProductResponse): Promise<> => {
    const get_Product_req = R.merge(GetProductRequest, {id: product_response.id});
    return product_boundary.get_product_details(in_memory_datastore, get_Product_req);
  });
  return Promise.all([
    get_Product_promise.should.eventually.be.fulfilled,
    get_Product_promise.should.eventually.have.property('name'),
    get_Product_promise.should.eventually.have.property('description'),
    get_Product_promise.should.eventually.have.property('product_id')
  ]);
});
