/* @flow */
'use strict';

import {create_product} from '../interactor/create_product_interactor';
import {update_product} from '../interactor/update_product_interactor';
import {get_product_from_db} from '../interactor/get_product_interactor';
import {type ProductDatastoreProvider} from '../datastore/product_datastore_provider';
import {CreateProductRequest} from '../request/create_product_request';
import {UpdateProductRequest} from '../request/update_product_request';
import {GetProductRequest} from '../request/get_product_request';

export function create_new_product(datastore: ProductDatastoreProvider, create_product_request: CreateProductRequest): Promise<* | Error> {
  return create_product(datastore, create_product_request);
}

export function update_product_details(datastore: ProductDatastoreProvider, update_product_request: UpdateProductRequest): Promise<* | Error> {
  return update_product(datastore, update_product_request);
}

export function get_product_details(datastore: ProductDatastoreProvider, request: GetProductRequest): Promise<*> {
  return get_product_from_db(datastore, request);
}
