/* @flow */
'use strict';


export interface ProductEntity {
  +id?: string,
  +name: string,
  +description: string
}

export const InitialProductEntity = {
  id: '',
  name: '',
  description: ''
};
