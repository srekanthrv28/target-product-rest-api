/* @flow */
'use strict';

import {UserAuthTokenEntity} from '../entity/user_auth_token_entity';
import * as datastore from 'memory-cache';
import R from 'ramda';
import {logger} from 'app_log';
import {InitialAppError} from 'tiny_error';

export function create_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return R.ifElse(R.curry(does_user_auth_token_already_exist), error_user_auth_token_already_exist, create_new_datastore_user_auth_token)(user_auth_token_entity);
}

export function revoke_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return R.ifElse(R.curry(does_user_auth_token_already_exist), revoke_user_auth_token, error_user_auth_token_doesnt_exist)(user_auth_token_entity);
}

export function get_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return R.ifElse(R.curry(does_user_auth_token_already_exist), get_user_auth_token_from_db, error_user_auth_token_doesnt_exist)(user_auth_token_entity);
}

function get_user_auth_token_from_db(user_auth_token_entity: UserAuthTokenEntity): UserAuthTokenEntity {
  return datastore.get(user_auth_token_entity.token_id);
}

function does_user_auth_token_already_exist(user_auth_token_entity: UserAuthTokenEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(user_auth_token_entity.token_id);
}

function create_new_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  const _x = logger.info("Creating new user_auth_token: ", R.toString(user_auth_token_entity));
  return Promise.resolve(datastore.put(user_auth_token_entity.token_id, user_auth_token_entity));
}

function error_user_auth_token_already_exist(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return Promise.reject(R.merge(InitialAppError, {reason: `UserAuthToken for user_id: ", ${user_auth_token_entity.user_id}," already exist...`, type: "INVALID_REQUEST"}));
}

function revoke_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  const _x = logger.info("Deleting the entry with token: ", R.toString(user_auth_token_entity));
  const _delete_token = datastore.del(user_auth_token_entity.token_id);
  return Promise.resolve(user_auth_token_entity);
}

function error_user_auth_token_doesnt_exist(user_auth_token_entity: UserAuthTokenEntity): Promise<Error> {
  const _x = logger.error("UserAuthToken with token: ", R.toString(user_auth_token_entity.token_id), " doesn't exist..." );
  return Promise.reject(R.merge(InitialAppError, {reason: `token: ", ${user_auth_token_entity.token_id}," doesn't  exist...`, type: "INVALID_REQUEST"}));
}
