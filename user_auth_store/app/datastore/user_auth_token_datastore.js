/* @flow */
'use strict';

import {UserAuthTokenEntity} from '../entity/user_auth_token_entity';
import * as in_memory_datastore from './in_memory_user_auth_token_datastore';
import {UserAuthTokenDatastoreProvider} from './user_auth_token_datastore_provider';
import R from 'ramda';
const default_datastore: UserAuthTokenDatastoreProvider = in_memory_datastore;


export function create_datastore_user_auth_token(datastore: UserAuthTokenDatastoreProvider, user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return get_datastore(datastore).create_datastore_user_auth_token(user_auth_token_entity);
}

export function get_datastore_user_auth_token(datastore: UserAuthTokenDatastoreProvider, user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return get_datastore(datastore).get_datastore_user_auth_token(user_auth_token_entity);
}

export function revoke_datastore_user_auth_token(datastore: UserAuthTokenDatastoreProvider, user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return get_datastore(datastore).revoke_datastore_user_auth_token(user_auth_token_entity);
}

function get_datastore(provided_datastore: UserAuthTokenDatastoreProvider): UserAuthTokenDatastoreProvider {
  return R.cond([
    [R.isNil, R.always(default_datastore)],
    [R.isEmpty, R.always(default_datastore)],
    [R.T, R.always(provided_datastore)]
  ])(provided_datastore);
}
