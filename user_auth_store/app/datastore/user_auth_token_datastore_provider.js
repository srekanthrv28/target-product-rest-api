/* @flow */
'use strict';

import {UserAuthTokenEntity} from '../entity/user_auth_token_entity';

export interface UserAuthTokenDatastoreProvider {
  create_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<>,
  get_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<>,
  revoke_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<>
}
