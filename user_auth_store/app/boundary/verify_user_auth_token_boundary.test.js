/* @flow */
'use strict';

import {it, describe} from 'mocha';
import * as boundary from './user_auth_token_boundary';
import {CreateUserAuthTokenRequest} from '../request/create_user_auth_token_request';
import {UserAuthTokenResponse} from '../response/user_auth_token_response';
import {InitialVerifyUserAuthTokenRequest} from '../request/verify_user_auth_token_request';
import * as in_memory_datastore from '../datastore/in_memory_user_auth_token_datastore';

import faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import jwt from 'jwt-simple';
import config from 'config';

const node_env = R.defaultTo("development", process.env.NODE_ENV)
export const jwt_secret = config.get(R.join('.', [node_env, 'Authentication.JwtTokenSecret']));

const get_create_user_auth_token_request = (): CreateUserAuthTokenRequest => {
  return {
    user_id: faker.random.uuid(),
    permissions: []
  }
};

function verify_user_auth_token(auth_token_response: UserAuthTokenResponse): Promise<> {
  const decoded_token = jwt.decode(auth_token_response.token, jwt_secret);
  const verify_request = R.merge(InitialVerifyUserAuthTokenRequest, {token: decoded_token.jti});
  return boundary.verify_user_auth_token(in_memory_datastore, verify_request);
}

const _create_and_verify_user_auth_token = describe("Create and Verify UserAuthToken - Boundary", (): void => {
  const verify_user_auth_token_test = it('Should create and verify user_auth_token', (): Promise<> => {
    const test_request: CreateUserAuthTokenRequest = get_create_user_auth_token_request();
    const verify_auth_token_promise: Promise<> = boundary.create_user_auth_token(in_memory_datastore, test_request)
      .then(verify_user_auth_token);
    return Promise.all ([
        verify_auth_token_promise.should.be.fulfilled,
        verify_auth_token_promise.should.eventually.have.property('token'),
        verify_auth_token_promise.should.eventually.have.property('permissions').to.equal(test_request.permissions),
        verify_auth_token_promise.should.eventually.have.property('token_verified').to.equal(true)
      ]);
  });
  return verify_user_auth_token_test;
});

const _verify_invalid_token = describe("Verify invalid token - Boundary", (): void => {
  const verify_user_auth_token_test = it('verification should fail with invalid token', (): Promise<> => {
    const verify_request = R.merge(InitialVerifyUserAuthTokenRequest, {token: faker.random.uuid()});
    const verify_promise = boundary.verify_user_auth_token(in_memory_datastore, verify_request);
    return Promise.all ([
        verify_promise.should.be.rejected
      ]);
  });
  return verify_user_auth_token_test;
});
