/* @flow */
'use strict';

import * as create_user_auth_token_interactor from '../interactor/create_user_auth_token_interactor';
import * as verify_user_auth_token_interactor from '../interactor/verify_user_auth_token_interactor';
import * as revoke_user_auth_token_interactor from '../interactor/revoke_user_auth_token_interactor';

import {CreateUserAuthTokenRequest} from '../request/create_user_auth_token_request';
import {VerifyUserAuthTokenRequest} from '../request/verify_user_auth_token_request';
import {RevokeUserAuthTokenRequest} from '../request/revoke_user_auth_token_request';
import {UserAuthTokenDatastoreProvider} from '../datastore/user_auth_token_datastore_provider';

export function create_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request_model: CreateUserAuthTokenRequest): Promise<> {
  return create_user_auth_token_interactor.create_new_user_auth_token(datastore, request_model);
}

export function verify_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request_model: VerifyUserAuthTokenRequest): Promise<> {
  return verify_user_auth_token_interactor.verify_user_auth_token(datastore, request_model);
}

export function revoke_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request_model: RevokeUserAuthTokenRequest): Promise<> {
  return revoke_user_auth_token_interactor.revoke_user_auth_token(datastore, request_model);
}
