/* @flow */
'use strict';

import {it, describe} from 'mocha';
import * as boundary from './user_auth_token_boundary';
import {CreateUserAuthTokenRequest} from '../request/create_user_auth_token_request';
import {UserAuthTokenResponse} from '../response/user_auth_token_response';
import * as in_memory_datastore from '../datastore/in_memory_user_auth_token_datastore';

import faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const get_create_user_auth_token_request = (): CreateUserAuthTokenRequest => {
  return {
    user_id: faker.random.uuid(),
    permissions: []
  }
};

const create_test_user_auth_token = (create_user_auth_token_request: CreateUserAuthTokenRequest): Promise<> => {
  const create_user_auth_token_promise: Promise<> = boundary.create_user_auth_token(in_memory_datastore, create_user_auth_token_request);
  return Promise.all ([
      create_user_auth_token_promise.should.be.fulfilled,
      create_user_auth_token_promise.should.eventually.have.property('token'),
      create_user_auth_token_promise.should.eventually.have.property('permissions').to.equal(create_user_auth_token_request.permissions),
      ]);
}

const _create_user_auth_token = describe("Create UserAuthToken - Boundary", (): void => {
  const _create_user_auth_token = it('Should create user_auth_token', (): Promise<> => {
    const test_request: CreateUserAuthTokenRequest = get_create_user_auth_token_request();
    return create_test_user_auth_token(test_request);
  });
  return _create_user_auth_token;
});

const _create_duplicate_user_auth_token = describe("Create Duplicate UserAuthToken - Boundary", (): void => {
  return it('Should not create duplicate user_auth_token', (): Promise<> => {
    const test_request: CreateUserAuthTokenRequest = get_create_user_auth_token_request();
    const create_user_auth_token_promise: Promise<> = boundary.create_user_auth_token(in_memory_datastore, test_request);
    return create_user_auth_token_promise.then((user_auth_token_response: UserAuthTokenResponse): Promise<> => {
      const create_dup_user_auth_token_promise: Promise<> = boundary.create_user_auth_token(in_memory_datastore, test_request);
      return Promise.all([
        create_dup_user_auth_token_promise.should.be.fulfilled,
        create_dup_user_auth_token_promise.should.eventually.have.property('token').to.not.equal(user_auth_token_response.token),
        create_dup_user_auth_token_promise.should.eventually.have.property('permissions').to.equal(test_request.permissions)
      ]);
    });
  });
});
