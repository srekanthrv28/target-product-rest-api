/* @flow */
'use strict';

export interface VerifiedUserAuthTokenResponse {
  +user_id: string,
  +token: string,
  +permissions: Array<string>,
  +token_verified: boolean
}

export const InitialVerifiedUserAuthTokenResponse: VerifiedUserAuthTokenResponse = {
  user_id: '',
  token: '',
  permissions: [],
  token_verified: false
};
