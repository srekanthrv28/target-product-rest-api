/* @flow */
'use strict';

export interface UserAuthTokenResponse {
  +user_id: string,
  +token: string,
  +expires_at: Date,
  +permissions: Array<string>
}

export const InitialUserAuthTokenResponse: UserAuthTokenResponse = {
  user_id: '',
  token: '',
  permissions: []
};
