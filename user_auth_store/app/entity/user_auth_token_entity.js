/* @flow */
'use strict';

export interface UserAuthTokenEntity {
  +user_id: string,
  +token: string,
  +token_id: string,
  +expires_at: Date,
  +permissions: Array<string>
}

export const InitialUserAuthTokenEntity: UserAuthTokenEntity = {
  user_id: '',
  expires_at: new Date(),
  token: '',
  token_id: '',
  permissions: []
};
