/* @flow */
'use strict';

import {
  CreateUserAuthTokenRequest,
  CreateUserAuthTokenRequestConstraints,
} from '../request/create_user_auth_token_request';

import {
  VerifyUserAuthTokenRequest,
  VerifyUserAuthTokenRequestConstraints
} from '../request/verify_user_auth_token_request';
import {
  RevokeUserAuthTokenRequest,
  RevokeUserAuthTokenRequestConstraints
} from '../request/revoke_user_auth_token_request';

import validate from 'validate.js';
import {InitialAppError} from 'tiny_error';
import R from 'ramda';

export function validate_create_user_auth_token_request_async(create_user_auth_token_request: CreateUserAuthTokenRequest): Promise<> {
  const validation_result: mixed = validate.validate(create_user_auth_token_request, CreateUserAuthTokenRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_user_auth_token_request);
  }
  return send_error(validation_result);
}

export function validate_verify_user_auth_token_request_async(verify_user_auth_token_request: VerifyUserAuthTokenRequest): Promise<> {
  const validation_result: mixed = validate.validate(verify_user_auth_token_request, VerifyUserAuthTokenRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(verify_user_auth_token_request);
  }
  return send_error(validation_result);
}

export function validate_revoke_user_auth_token_request_async(revoke_user_auth_token_request: RevokeUserAuthTokenRequest): Promise<> {
  const validation_result: mixed = validate.validate(revoke_user_auth_token_request, RevokeUserAuthTokenRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(revoke_user_auth_token_request);
  }
  return send_error(validation_result);
}

function send_error(validation_result: mixed): Promise<>  {
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}
