/* @flow */
'use strict';

export interface RevokeUserAuthTokenRequest {
  +token: string
}

export const InitialRevokeUserAuthTokenRequest: RevokeUserAuthTokenRequest = {
  token: ''
};

export const RevokeUserAuthTokenRequestConstraints  = {
  token: {
    presence: true,
    length: {
      minimum: 1,
      maximum: 512
    }
  }
}
