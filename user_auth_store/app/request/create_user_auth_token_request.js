/* @flow */
'use strict';

export interface CreateUserAuthTokenRequest {
  +user_id: string,
  +permissions: Array<string>
}

export const InitialCreateUserAuthTokenRequest: CreateUserAuthTokenRequest = {
  user_id: '',
  permissions: []
};

export const CreateUserAuthTokenRequestConstraints  = {
  user_id: {
    presence: true,
    length: {
      minimum: 1,
      maximum: 512
    }
  },
  permissions: {}
}
