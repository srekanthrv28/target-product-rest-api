/* @flow */
'use strict';

export interface VerifyUserAuthTokenRequest {
  +token: string
}

export const InitialVerifyUserAuthTokenRequest: VerifyUserAuthTokenRequest = {
  token: ''
};

export const VerifyUserAuthTokenRequestConstraints  = {
  token: {
    presence: true,
    length: {
      maximum: 512
    }
  }
}
