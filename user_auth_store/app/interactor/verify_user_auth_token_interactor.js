/* @flow */
'use strict';

import {logger} from 'app_log';
import {UserAuthTokenEntity} from '../entity/user_auth_token_entity';
import {VerifyUserAuthTokenRequest} from '../request/verify_user_auth_token_request';
import {VerifiedUserAuthTokenResponse, InitialVerifiedUserAuthTokenResponse} from '../response/verified_user_auth_token_response';
import * as user_auth_token_datastore from "../datastore/user_auth_token_datastore";
import * as request_validator from '../request/user_auth_token_request_validator';
import * as user_auth_token_entity_wrapper from './user_auth_token_entity_wrapper';
import {UserAuthTokenDatastoreProvider} from '../datastore/user_auth_token_datastore_provider';
import R from 'ramda';

export function verify_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request: VerifyUserAuthTokenRequest): Promise<> {
  const _x = logger.info(`got the request to verify the user_auth_token details with request: ${R.toString(request)}`);
  return request_validator.validate_verify_user_auth_token_request_async(request)
    .then(user_auth_token_entity_wrapper.from_verify_user_auth_token_request_to_datastore_entity)
    .then(R.curry(user_auth_token_datastore.get_datastore_user_auth_token)(datastore))
    .then(verify_fetched_user_auth_token);
}

function verify_fetched_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): VerifiedUserAuthTokenResponse {
  if (R.isNil(user_auth_token_entity)) {
    return InitialVerifiedUserAuthTokenResponse;
  }
  const verified_resp = R.pick(R.keys(InitialVerifiedUserAuthTokenResponse), user_auth_token_entity);
  return R.merge(verified_resp, {token_verified: true});
}
