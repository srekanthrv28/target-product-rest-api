/* @flow */
'use strict';

import {logger} from 'app_log';
import {CreateUserAuthTokenRequest} from "../request/create_user_auth_token_request";
import {UserAuthTokenResponse} from "../response/user_auth_token_response";
import {UserAuthTokenEntity} from '../entity/user_auth_token_entity';

import * as user_auth_token_datastore from "../datastore/user_auth_token_datastore";
import * as request_validator from '../request/user_auth_token_request_validator';
import * as user_auth_token_entity_wrapper from './user_auth_token_entity_wrapper';
import {UserAuthTokenDatastoreProvider} from '../datastore/user_auth_token_datastore_provider';
import R from 'ramda';

export function create_new_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request: CreateUserAuthTokenRequest): Promise<UserAuthTokenResponse | Error> {
  const _x = logger.info(`got the request to create the user_auth_token: ${R.toString(request)}`);
  return request_validator.validate_create_user_auth_token_request_async(request)
    .then(user_auth_token_entity_wrapper.from_user_auth_token_request_to_datastore_entity)
    .then(user_auth_token_entity_wrapper.encode_and_sign_token_for_user)
    .then(R.curry(user_auth_token_datastore.create_datastore_user_auth_token)(datastore))
    .then(user_auth_token_creation_success);
}

function user_auth_token_creation_success(user_auth_token_entity: UserAuthTokenEntity): UserAuthTokenResponse {
  const _x = logger.info(`Successfully created user_auth_token: ${R.toString(user_auth_token_entity)}`);
  return user_auth_token_entity_wrapper.from_user_auth_token_entity_to_response(user_auth_token_entity);
}
