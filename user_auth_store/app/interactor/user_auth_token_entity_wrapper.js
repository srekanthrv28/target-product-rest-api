/* @flow */
'use strict';

import {CreateUserAuthTokenRequest} from "../request/create_user_auth_token_request";
import {VerifyUserAuthTokenRequest} from '../request/verify_user_auth_token_request';
import {RevokeUserAuthTokenRequest} from "../request/revoke_user_auth_token_request";
import {InitialUserAuthTokenResponse, UserAuthTokenResponse} from "../response/user_auth_token_response";
import {InitialUserAuthTokenEntity, UserAuthTokenEntity} from '../entity/user_auth_token_entity';
import R from 'ramda';
import moment from 'moment';
import jwt from 'jwt-simple';
import config from 'config';
import uuidV4 from 'uuid/v4';
const node_env = R.defaultTo("development", process.env.NODE_ENV)
export const jwt_secret = config.get(R.join('.', [node_env, 'Authentication.JwtTokenSecret']));
export const expire_days = config.get(R.join('.', [node_env, 'Authentication.expire_days']));

interface JwtToken {
  jti: string,
  token: string,
  iat: number,
  sub: string
}

export function from_user_auth_token_entity_to_response(user_auth_token_entity: UserAuthTokenEntity): UserAuthTokenResponse {
  return R.pick(R.keys(InitialUserAuthTokenResponse), user_auth_token_entity);
}

export function from_user_auth_token_request_to_datastore_entity(request: CreateUserAuthTokenRequest): UserAuthTokenEntity {
  return R.pick(R.keys(InitialUserAuthTokenEntity), request);
}

export function from_verify_user_auth_token_request_to_datastore_entity(request: VerifyUserAuthTokenRequest): Promise<> {
  return R.pick(R.keys(InitialUserAuthTokenEntity), {token_id: request.token});
}

export function from_revoke_token_request_to_datastore_entity(request: RevokeUserAuthTokenRequest): Promise<> {
  return R.pick(R.keys(InitialUserAuthTokenEntity), {token_id: request.token});
}

export function from_revoke_user_auth_token_request_to_datastore_entity(request: RevokeUserAuthTokenRequest): UserAuthTokenEntity {
  return R.pick(R.keys(InitialUserAuthTokenEntity), request);
}

export function encode_and_sign_token_for_user(entity: UserAuthTokenEntity): UserAuthTokenEntity {
  const jwt_token = build_token_for_user(entity.user_id, entity.permissions, entity.role);
  return R.merge(entity, {token: jwt_token.token, token_id: jwt_token.jti, expires_at: jwt_token.iat});
}

export function decode_auth_token_to_entity(jwt_token: string): UserAuthTokenEntity {
  const decoded_token = jwt.decode(jwt_token, jwt_secret);
  return R.merge(InitialUserAuthTokenEntity, {
    user_id: decoded_token.sub,
    token: jwt_token,
    token_id: decoded_token.jti,
    role: decoded_token.role,
    permissions: decoded_token.permissions
  });
}


function build_token_for_user(user_id: string, permissions: Array<string>, role: string): JwtToken {
  const iat_timestamp = moment().unix();
  const nbf = moment().unix();
  const expire_timestamp = moment().add(expire_days, 'days').unix()
  const jwt_token = jwt.encode({ sub: user_id, permissions: permissions, role: role, jti: uuidV4(), iat: iat_timestamp, exp: expire_timestamp, nbf: nbf}, jwt_secret);
  const decoded_token = jwt.decode(jwt_token, jwt_secret);
  return {
    jti: decoded_token.jti,
    token: jwt_token,
    iat: iat_timestamp,
    expires_at: expire_timestamp,
    sub: user_id
  };
}
