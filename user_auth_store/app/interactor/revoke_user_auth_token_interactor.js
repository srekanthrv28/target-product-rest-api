/* @flow */
'use strict';

import {logger} from 'app_log';
import R from 'ramda';

import {
  type RevokeUserAuthTokenRequest
} from '../request/revoke_user_auth_token_request';
import {
  type UserAuthTokenEntity
} from '../entity/user_auth_token_entity';
import {
  InitialUserAuthTokenResponse,
  type UserAuthTokenResponse
} from '../response/user_auth_token_response';
import * as request_validator from '../request/user_auth_token_request_validator';
import * as user_auth_token_datastore from "../datastore/user_auth_token_datastore";
import * as user_auth_token_entity_wrapper from './user_auth_token_entity_wrapper';
import {UserAuthTokenDatastoreProvider} from '../datastore/user_auth_token_datastore_provider';

export function revoke_user_auth_token(datastore: UserAuthTokenDatastoreProvider, request: RevokeUserAuthTokenRequest): Promise<> {
  const _x = logger.info(`got the request to revoke the user_auth_token with request: ${R.toString(request)}`);
  return request_validator.validate_revoke_user_auth_token_request_async(request)
    .then(user_auth_token_entity_wrapper.from_revoke_token_request_to_datastore_entity)
    .then(R.curry(user_auth_token_datastore.revoke_datastore_user_auth_token)(datastore))
    .then(user_auth_token_revoke_success);
}

function user_auth_token_revoke_success(user_auth_token_entity: UserAuthTokenEntity): UserAuthTokenResponse {
  return R.pick(R.keys(InitialUserAuthTokenResponse), user_auth_token_entity);
}
