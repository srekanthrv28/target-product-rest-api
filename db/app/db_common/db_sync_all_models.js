/* @flow */
'use strict';

import UserDbEntity from '../user_db/user_db_model/user_db_entity';
import ProductPriceDbEntity from '../product_price_db/product_price_db_model/product_price_db_entity';
import UserAuthTokenDbEntity from '../user_auth_db/user_auth_db_model/user_auth_token_db_entity';
import ProductDbEntity from '../product_db/product_db_model/product_db_entity';

import {db} from '../db_init/db';

export function define_all_models(): Promise<> {
  return db.authenticate()
    .then(UserDbEntity.sync())
    .then(UserAuthTokenDbEntity.sync())
    .then(ProductPriceDbEntity.sync())
    .then(ProductDbEntity.sync());
}
