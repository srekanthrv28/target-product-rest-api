/* @flow */
'use strict';

import {type UserDatastoreProvider} from 'user';
import {type ProductPriceDatastoreProvider} from 'product_price';
import {type ProductDatastoreProvider} from 'product';
import {is_db_enabled} from './db_config';
import * as user_db_datastore from '../user_db/user_db_datastore/user_db_datastore';
import * as product_price_db_datastore from '../product_price_db/product_price_db_datastore/product_price_db_datastore';
import * as user_auth_token_db_datastore from '../user_auth_db/user_auth_db_datastore/user_auth_db_datastore';
import * as product_db_datastore from '../product_db/product_db_datastore/product_db_datastore';


export function get_user_db_datastore(): UserDatastoreProvider {
  if (is_db_enabled()) {
    return user_db_datastore;
  }
  return {};
}

export function get_product_price_db_datastore(): ProductPriceDatastoreProvider {
  if (is_db_enabled()) {
    return product_price_db_datastore;
  }
  return {};
}

export function get_user_auth_token_db_datastore(): UserAuthTokenDatastoreProvider {
  if (is_db_enabled()) {
    return user_auth_token_db_datastore;
  }
  return {};
}

export function get_product_db_datastore(): ProductDatastoreProvider {
  if (is_db_enabled()) {
    return product_db_datastore;
  }
  return {};
}
