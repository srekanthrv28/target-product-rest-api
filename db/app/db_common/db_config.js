/* @flow */
'use strict';

import R from 'ramda';

export function is_db_enabled(): boolean {
  return R.equals('true')(R.toLower(R.defaultTo('false', process.env.DB_ENABLED)));
}
