/* @flow */
'use strict';

import {DataTypes} from 'sequelize';
import {db} from '../../db_init/db';
import R from 'ramda';
import bcrypt from 'bcrypt';

const user_db_entity_fields = {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    validate: {notEmpty: true}
  },
  email: {
    type: DataTypes.STRING,
    allowNull: true,
    validate: {isEmail: true},
    unique: 'unique_user_email_index'
  },
 password: {
    type: DataTypes.STRING,
    allowNull: true,
    set(password: string): string {
      return this.setDataValue('password', encript_password(password)); //eslint-disable-line immutable/no-this, fp/no-this
    }
  },
  status: {
    type: DataTypes.ENUM("active", "inactive"),
    allowNull: false,
    defaultValue: "active"
  }
}

const user_db_entity_config = {
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  paranoid: true,
  charset: 'utf8',
  collate:'utf8_unicode_ci'
};


const UserDbEntity = db.define('user', user_db_entity_fields, user_db_entity_config);
export default UserDbEntity;


function encript_password(password: string): string {
  return R.cond([
    [R.isNil, R.always(null)], //eslint-disable-line fp/no-nil
    [R.isEmpty, R.always(null)], //eslint-disable-line fp/no-nil
    [R.T, (value: string): string => R.curry(get_password_hash)(value)],
  ])(password);
}


function get_password_hash(password: string): string {
  return bcrypt.hashSync(password, 10);
}
