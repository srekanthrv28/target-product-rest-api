/* @flow */
'use strict';

import {it, before} from 'mocha';
import faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);

import UserDbEntity from './user_db_entity';
import {UserEntity} from 'user';
import {define_all_models} from '../../db_common/db_sync_all_models';

function get_test_create_user_request(): UserEntity {
  return {
    user_role: faker.random.word(),
    email: faker.internet.email(),
    password: faker.internet.password()
  };
}

const _before_test = before((): Promise<> => define_all_models());

const _create_test_user = it('Should Add user entry to database', (): Promise<> => {
  const test_user_request = get_test_create_user_request();
  const create_user_promise = UserDbEntity.create(test_user_request);
  return Promise.all([
    create_user_promise.should.eventually.be.fulfilled
  ]);
});
