/* @flow */
'use strict';

import {type UserEntity} from 'user';
import R from 'ramda';
import {logger} from 'app_log';
import {InitialAppError} from 'tiny_error';
import UserDbEntity from '../user_db_model/user_db_entity';
const inactive_status = 'inactive';

export function create_datastore_user(user_entity: UserEntity): Promise<*> {
  return get_user(user_entity)
    .then(R.curry(handle_get_user)(user_entity));
}

export function get_user(user_entity: UserEntity): Promise<*> {
  const found_user = UserDbEntity.findOne(
      {
        where: {
            $or: [
                  {email: {$eq: user_entity.email}}
                ]
          }});
    return found_user;
}

function handle_get_user(user_entity: UserEntity, user_db_entity: UserDbEntity): Promise<*> {
  if (user_db_entity && R.equals(user_db_entity.email, user_entity.email)) {
    return error_user_with_email_already_exist(user_db_entity);
  } 
  return create_new_datastore_user(user_entity);
}

export function create_new_datastore_user_without_password(user_entity: UserEntity): Promise<UserEntity> {
  return get_user(user_entity)
    .then(R.curry(handle_get_user)(user_entity));
}

function create_new_datastore_user(user_entity: UserEntity): Promise<UserEntity> {
  const _a = logger.info("Creating new user in db with data: ", user_entity);
  if(user_entity.email) {
    const new_user_entity = {
      email: R.toLower(user_entity.email),
      password: user_entity.password
    };
    return UserDbEntity.create(new_user_entity);
  }
  return UserDbEntity.create(user_entity);
}

function error_user_with_email_already_exist(user_db_entity: UserDbEntity): Promise<Error> {
  const _a = logger.error(`User with email ${R.toString(user_db_entity.email)} already exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `User with email: ${user_db_entity.email} already exists`, type: "INVALID_REQUEST"}));
}

export async function find_datastore_user_by_email(user_entity: UserEntity): Promise<UserEntity | Error> {
  const user = await UserDbEntity.findOne({where: {email: R.toLower(user_entity.email)}});
  if(R.isNil(user) || R.isEmpty(user)) {
    return user;
  }
  const plain_user = await user.get({plain: true});
  if(R.equals(plain_user.status, inactive_status)) {
   return Promise.reject(R.merge(InitialAppError, {reason: `User with email: ${plain_user.email} is not active`, type: "INVALID_REQUEST"}));
  }
  return user;
}

export function find_datastore_user_by_user_id(user_entity: UserEntity): Promise<UserEntity | Error> {
  return UserDbEntity.findOne({where: {id: user_entity.id}});
}
