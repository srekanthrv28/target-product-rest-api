/* @flow */
'use strict';

import {it, before} from 'mocha';

import faker from 'faker';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';

import {UserEntity} from 'user';
import {define_all_models} from '../../db_common/db_sync_all_models';
import * as user_db_datastore from './user_db_datastore';

function get_test_create_user_request(): UserEntity {
  return {
    user_role: faker.random.word(),
    email: faker.internet.email(),
    password: faker.internet.password()
  };
}

const _before_test = before((): Promise<> => define_all_models());

const _create_test_user = it('Should create a new user', (): Promise<> => {
  const test_user_request = get_test_create_user_request();
  const create_user_promise = user_db_datastore.create_datastore_user(test_user_request);
  return Promise.all([
    create_user_promise.should.eventually.be.fulfilled,
    create_user_promise.should.eventually.have.property('id'),
    create_user_promise.should.eventually.have.property('email').equal(R.toLower(test_user_request.email)),
      create_user_promise.should.eventually.have.property('created_at'),
    create_user_promise.should.eventually.have.property('updated_at')
  ]);
});
