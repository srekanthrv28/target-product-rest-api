/* @flow */
'use strict';

import {DataTypes} from 'sequelize';
import {db} from '../../db_init/db';

const user_auth_token_db_entity_fields = {
  token: {
    type: DataTypes.STRING(2048),
    allowNull: false
  },
  jti: {
    type: DataTypes.STRING,
    allowNull: false,
    unique: true
  },
  sub: {
    type: DataTypes.STRING,
    allowNull: true
  },
  iat: {
    type: DataTypes.BIGINT,
    allowNull: false
  },
  status: {
    type: DataTypes.ENUM("active", "inactive"),
    allowNull: false,
    defaultValue: "active"
  }
}

const user_auth_token_db_entity_config = {
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  paranoid: true,
  charset: 'utf8',
  collate:'utf8_unicode_ci'
};


const UserAuthTokenDbEntity = db.define('user_auth_token', user_auth_token_db_entity_fields, user_auth_token_db_entity_config);

export default UserAuthTokenDbEntity;
