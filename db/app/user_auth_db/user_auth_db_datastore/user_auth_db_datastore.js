/* @flow */
'use strict';

import {type UserAuthTokenEntity, user_auth_token_entity_wrapper} from 'user_auth_store';
import R from 'ramda';
import {logger} from 'app_log';
import UserAuthTokenDbEntity from '../user_auth_db_model/user_auth_token_db_entity';

export function create_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return get_user_auth_token_by_jti(user_auth_token_entity.token_id)
    .then(R.curry(handle_get_user_auth_token_for_create_new)(user_auth_token_entity))
    .then(handle_get_user_auth_token_result);
}

export function revoke_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<> {
  return get_user_auth_token_by_jti(user_auth_token_entity.token_id)
    .then(R.curry(handle_get_user_auth_token_for_revoke)(user_auth_token_entity));
}

export function get_datastore_user_auth_token(user_auth_token_db_entity: UserAuthTokenDbEntity): Promise<> {
  return get_user_auth_token_by_jti(user_auth_token_db_entity.token_id)
    .then(handle_get_user_auth_token_result);
}

function get_user_auth_token_by_jti(jti: string): Promise<> {
  return UserAuthTokenDbEntity.findOne({where: {jti: jti}});
}

function handle_get_user_auth_token_result(user_auth_token_db_entity: UserAuthTokenDbEntity): UserAuthTokenEntity {
  if (R.isNil(user_auth_token_db_entity)) {
    return user_auth_token_db_entity;
  }
  const parsed_entity_from_token = user_auth_token_entity_wrapper.decode_auth_token_to_entity(user_auth_token_db_entity.token);
  return R.merge(parsed_entity_from_token, {
    token: user_auth_token_db_entity.token,
    token_id: user_auth_token_db_entity.jti,
    expires_at: user_auth_token_db_entity.iat,
    user_id: user_auth_token_db_entity.sub
  });
}

function handle_get_user_auth_token_for_create_new(user_auth_token_entity: UserAuthTokenEntity, user_auth_token_db_entity: UserAuthTokenDbEntity): Promise<> {
  if (user_auth_token_db_entity) {
    const _get_user_lang_info = logger.error("Token alrady exists in database: ", R.toString(user_auth_token_db_entity));
    return Promise.reject("Invalid Token. Token already exists: " + user_auth_token_entity.token_id);
  }
  return create_new_datastore_user_auth_token(user_auth_token_entity);
}

function handle_get_user_auth_token_for_revoke(user_auth_token_entity: UserAuthTokenEntity, user_auth_token_db_entity: UserAuthTokenDbEntity): Promise<> {
  if (user_auth_token_db_entity) {
    const _get_user_lang_info = logger.error("Revoking token: ", R.toString(user_auth_token_db_entity));
    return UserAuthTokenDbEntity.destroy({where: {jti: user_auth_token_entity.token_id}})
      .then((_deleted_count: number): Promise<> => {
        return Promise.resolve(user_auth_token_entity);
      });
  }
  return Promise.reject("Invalid token: " + user_auth_token_entity.token_id);
}

function create_new_datastore_user_auth_token(user_auth_token_entity: UserAuthTokenEntity): Promise<UserAuthTokenEntity> {
  const _a = logger.info("Creating new user auth token with data: ", user_auth_token_entity);
  const auth_token_db_entity = convert_user_auth_token_entity_to_db_entity(user_auth_token_entity);
  return UserAuthTokenDbEntity.create(auth_token_db_entity);
}

function convert_user_auth_token_entity_to_db_entity(user_auth_token_entity: UserAuthTokenEntity): UserAuthTokenDbEntity {
  return {
    token: user_auth_token_entity.token,
    jti: user_auth_token_entity.token_id,
    sub: user_auth_token_entity.user_id,
    iat: user_auth_token_entity.expires_at
  };
}
