/* @flow */
'use strict';

import {DataTypes} from 'sequelize';
import {db} from '../../db_init/db';

const product_price_db_entity_fields = {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    validate: {notEmpty: true}
  },
  product_id: {
    type: DataTypes.UUID,
    references: {
      model: 'product',
      key: 'id'
    },
    unique: false,
    allowNull: false
  },
  value:{
    type: DataTypes.DOUBLE,
    allowNull: true
  },
  currency_code: {
    type: DataTypes.STRING(1024),
    allowNull: true
  },
  status: {
    type: DataTypes.ENUM("active", "inactive"),
    allowNull: false,
    defaultValue: "active"
  }
}

const product_price_db_entity_config = {
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  paranoid: true,
  charset: 'utf8',
  collate:'utf8_unicode_ci'
};


const ProductPriceDbEntity = db.define('product_price', product_price_db_entity_fields, product_price_db_entity_config);

export default ProductPriceDbEntity;
