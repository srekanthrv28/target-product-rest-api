/* @flow */
'use strict';

import {type ProductPriceEntity} from 'product_price';
import R from 'ramda';
import {logger} from 'app_log';
import ProductPriceDbEntity from '../product_price_db_model/product_price_db_entity';

export function create_or_update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<*> {
  return get_product_price_by_product_id(product_price_entity.product_id)
    .then(R.curry(handle_get_product_price_for_create_new)(product_price_entity));
}

export function update_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<*> {
  return get_product_price_by_product_id(product_price_entity.product_id)
    .then(R.curry(handle_get_product_price_for_update)(product_price_entity));
}

export function get_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<*> {
  return get_product_price_by_product_id(product_price_entity.product_id);
}

function get_product_price_by_product_id(product_id: string): Promise<*> {
  return ProductPriceDbEntity.findOne({where: {product_id: product_id}});
}

function handle_get_product_price_for_create_new(product_price_entity: ProductPriceEntity, product_price_db_entity: ProductPriceDbEntity): Promise<*> {
  if (product_price_db_entity) {
    const _log_product_price_get = logger.info("Got product price from database: ", R.toString(product_price_db_entity));
    return update_product_price(product_price_db_entity, product_price_entity);
  }
  return create_new_datastore_product_price(product_price_entity);
}

function create_new_datastore_product_price(product_price_entity: ProductPriceEntity): Promise<ProductPriceEntity> {
  const _a = logger.info("Creating new product price with data: ", product_price_entity);
  return ProductPriceDbEntity.create(product_price_entity);
}

function handle_get_product_price_for_update(product_price_entity: ProductPriceEntity, product_price_db_entity: ProductPriceDbEntity): Promise<*> {
  if (product_price_db_entity) {
    const _get_product_price_log = logger.info("Got product price from database to update: ", R.toString(product_price_db_entity));
    return update_product_price(product_price_db_entity, product_price_entity);
  }
  return create_new_datastore_product_price(product_price_entity);
}

function update_product_price(existing_product_price_db_entity: ProductPriceDbEntity, product_price_entity: ProductPriceEntity): Promise<*> {
  const updated_product_price = R.merge(existing_product_price_db_entity, product_price_entity);
  return existing_product_price_db_entity.update(updated_product_price);
}
