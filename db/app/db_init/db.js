/* @flow */
'use strict';

import config from 'config';
import Sequelize from 'sequelize';
import {logger} from 'app_log';
import R from 'ramda';

const node_env = R.defaultTo("development", process.env.NODE_ENV)
const db_config = config.get(R.join('.', [node_env, 'database']));

const _log_before_db_init = logger.debug("Initializing the Database...");
export const db = new Sequelize(
  db_config.database,
  db_config.username,
  db_config.password,
  db_config
);
const _log_after_db_init = logger.debug("Initialized the Database!!");

export default db;
