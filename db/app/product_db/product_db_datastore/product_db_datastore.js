/* @flow */
'use strict';

import {type ProductEntity, InitialProductEntity} from 'product';
import R from 'ramda';
import {logger} from 'app_log';
import {InitialAppError} from 'tiny_error';
import ProductDbEntity from '../product_db_model/product_db_entity';

export function create_datastore_product(product_entity: ProductEntity): Promise<*> {
  return get_product(product_entity)
        .then(R.curry(handle_get_product)(product_entity));
}


export function update_datastore_product(product_entity: ProductEntity): Promise<*> {
  return get_product_by_id(product_entity)
        .then(R.curry(handle_update_product)(product_entity));
}

export function get_product(product_entity: ProductEntity): Promise<*> {
  const found_product = ProductDbEntity.findOne({where: {id:  product_entity.product_id}});
  return found_product;
}

export function get_product_by_id(product_entity: ProductEntity): Promise<*> {
  const found_product = ProductDbEntity.findOne({where: {id:  product_entity.product_id}});
  return found_product;
}

export async function get_product_from_db(product_request: ProductEntity): Promise<*> {
  const product_entity = await ProductDbEntity.findOne({where: {id: product_request.product_id}});
  if(R.isNil(product_entity) || R.isEmpty(product_entity)) {
    return error_product_does_not_extis(product_request);
  }
  return product_entity.get({plain: true});
}

export async function get_all_agencies_from_db(): Promise<Array<ProductDbEntity>> {
  const product_list = await ProductDbEntity.findAll();
  return handle_agencies_list_result(product_list);
}

function handle_agencies_list_result(product_entity_list: Array<ProductDbEntity>): Array<ProductEntity> {
  return R.map(get_plain_product_details, product_entity_list);
}

function get_plain_product_details(product_entity: ProductDbEntity): mixed {
  const product_plain_entity = product_entity.get({plain: true});
  return R.pick(R.keys(InitialProductEntity), product_plain_entity);
}

function handle_get_product(product_entity: ProductEntity, product_db_entity: ProductDbEntity): Promise<> {
  if (product_db_entity && R.equals(product_db_entity.product_id, product_entity.product_id)) {
    return error_product_with_product_id_already_exist(product_db_entity);
  }
  return create_new_datastore_product(product_entity);
}

function handle_update_product(product_entity: ProductEntity, product_db_entity: ProductDbEntity): Promise<> {
  if (product_db_entity) {
    return update_product(product_db_entity, product_entity);
  }
  return error_product_does_not_extis(product_entity);
}

function create_new_datastore_product(product_entity: ProductEntity): Promise<ProductEntity> {
  const _a = logger.info("Creating new product in db with data: ", product_entity);
  return ProductDbEntity.create(product_entity);
}

function update_product(existing_product_db_entity: ProductDbEntity, product_entity: ProductEntity): Promise<> {
  const updated_product = R.merge(existing_product_db_entity, product_entity);
  return existing_product_db_entity.update(updated_product);
}

function error_product_with_product_id_already_exist(product_db_entity: ProductDbEntity): Promise<Error> {
  const _a = logger.error(`Product with product_id ${R.toString(product_db_entity.product_id)} already exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `Product with product_id: ${product_db_entity.product_id} already exists`, type: "INVALID_REQUEST"}));
}

function error_product_does_not_extis(product_entity: *): Promise<Error> {
  const _a = logger.error(`Product with id ${R.toString(product_entity.product_id)} doesn't exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `Product with id: ${product_entity.product_id} doesn't exists.`, type: "INVALID_REQUEST"}));
}
