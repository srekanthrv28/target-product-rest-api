/* @flow */
'use strict';

import {DataTypes} from 'sequelize';
import {db} from '../../db_init/db';

const product_db_entity_fields = {
  id: {
    type: DataTypes.UUID,
    allowNull: false,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
    validate: {notEmpty: true}
  },
  name: {
    type: DataTypes.STRING,
    allowNull: true
  },
  description: {
    type: DataTypes.STRING,
    allowNull: true
  },
  status: {
    type: DataTypes.ENUM("active", "inactive"),
    allowNull: false,
    defaultValue: "active"
  }
};

const product_db_entity_config = {
  timestamps: true,
  underscored: true,
  freezeTableName: true,
  paranoid: true,
  charset: 'utf8',
  collate:'utf8_unicode_ci'
};


const ProductDbEntity = db.define('product', product_db_entity_fields, product_db_entity_config);
export default ProductDbEntity;
