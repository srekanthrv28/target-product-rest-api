'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('product_price', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
        },
      product_id: {
        type: Sequelize.UUID,
        references: {
          model: 'product',
          key: 'id'
        },
        unique: false,
        allowNull: false,
        onUpdate: 'cascade',
        onDelete: 'cascade'
      },
      value: {
        type: Sequelize.DOUBLE,
        allowNull: true
      },
      currency_code: {
        type: Sequelize.STRING,
        allowNull: true
      },
      status: {
        type: Sequelize.ENUM("active", "inactive"),
        allowNull: false,
        defaultValue: "active"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    },
    {
      charset: 'utf8',
      collate:'utf8_unicode_ci'
    });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('product_price');
  }
};
