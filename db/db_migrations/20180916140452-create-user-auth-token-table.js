'use strict';

module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('user_auth_token', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      token: {
        type: Sequelize.STRING(2048),
        allowNull: false
      },
      jti: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true
      },
      sub: {
        type: Sequelize.STRING,
        allowNull: true
      },
      iat: {
        type: Sequelize.BIGINT,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM("active", "inactive"),
        allowNull: false,
        defaultValue: "active"
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
        defaultValue: Sequelize.literal('NOW()')
      },
      deleted_at: {
        allowNull: true,
        type: Sequelize.DATE
      }
    }, {charset: 'utf8',collate:'utf8_unicode_ci'});
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('user_auth_token');
  }
};
