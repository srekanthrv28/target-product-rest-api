/* @flow */
'use strict';

export interface AppError {
  +reason: string,
  +type: string,
  +fields: Array<mixed>
}

export const InitialAppError: AppError = {
  name: "TINY_ERROR",
  reason: '',
  type: '',
  fields: []
};

export function is_custom_error(error: *): boolean {
  if (error) {
    return error.name === 'TINY_ERROR';
  }
  return false;
}
