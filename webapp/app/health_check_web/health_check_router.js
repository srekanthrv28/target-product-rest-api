/* @flow */
'use strict';

import * as express from 'express';
import * as health_check_controller from './health_check_controller';
const router: express.Router = express.Router();


/**
 * @swagger
 * /health_check:
 *   get:
 *     tags:
 *       - Health Check
 *     description: Get Health Check Result
 *     parameters:
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Get Health Check Result
 */
const _get_health_check_result = router.get('/',  (req: express$Request, res: express$Response): number => {
  const _updated_response = health_check_controller.get_health_check_response(req, res);
  return 0;
});


export {
  router
};
