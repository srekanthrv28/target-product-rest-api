/* @flow */
'use strict';

import {type $Request as ExpressRequest, type $Response as ExpressResponse} from 'express';
import * as web_response_wrapper from '../core_web/web_response_wrapper';
import R from 'ramda';
const api_version = "1.0";
const status = "Up and Running"


export function get_health_check_response(request: ExpressRequest, response: ExpressResponse): Promise<> {
   return get_health_check_result()
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}


function get_health_check_result(): Promise<*> {
   return Promise.resolve({
     status: status,
     api_version: api_version
   });
}
