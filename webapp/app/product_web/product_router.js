                                                                             /* @flow */
'use strict';

import * as express from 'express';
import type {$Request, $Response} from 'express';
import * as product_controller from './product_controller';
import {require_auth} from '../user_auth_web/user_auth_passport_service';

const router: express.Router = express.Router();

/**
 * @swagger
 * /product:
 *   post:
 *     tags:
 *       - Product
 *     description: Create Product
 *     parameters:
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          Product Response
 */

const _product_create = router.post('/', require_auth, (req: $Request, res: $Response): number => {
  const _updated_response = product_controller.create_product(req, res);
  return 0;
});


/**
 * @swagger
 * /product:
 *   get:
 *     tags:
 *       - Product
 *     description: Get Product
 *     parameters:
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          Product Response
 */

const _get_product = router.get('/:product_id', require_auth, (req: $Request, res: $Response): number => {
  const _updated_response = product_controller.get_product(req, res);
  return 0;
});


/**
 * @swagger
 * /product:
 *   put:
 *     tags:
 *       - Product
 *     description: update Product
 *     parameters:
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *          Product Response
 */

const _update_product = router.put('/:product_id', require_auth, (req: $Request, res: $Response): number => {
  const _updated_response = product_controller.update_product(req, res);
  return 0;
});


export {
  router
};
