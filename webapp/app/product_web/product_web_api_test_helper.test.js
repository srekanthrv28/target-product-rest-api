/* @flow */
'use strict';
import faker from 'faker';
import {type $Response as ExpressResponse} from 'express';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
import webapp_server from '../core_web/webapp_server';
import {CreateProductWebRequest} from './web_request/create_product_web_request';
import {InitialGetProductAddressWebRequest} from './web_request/get_product_web_request';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);
import _moment_timezone from 'moment-timezone';
import R from 'ramda';
//import {logger} from 'app_log';

const password = "test@123";
const product_email = faker.internet.email();

export function get_admin_credentials(): *{
  const admin_credentials = {
    email: "admin@target.co",
    password: "target@123"
  }
  return admin_credentials;
}

export function get_product_credentials(): *{
  const product_credentials = {
    email: product_email,
    password: password
  }
  return product_credentials;
}

export function get_test_create_product_request(): CreateProductWebRequest {
  const test_create_product_request = {
    name: R.toLower(faker.name.firstName()),
    description: faker.name.firstName()
  }
  return test_create_product_request;
}

export function login_admin(login_request: *): Promise<*> {
  return chai.request(webapp_server)
        .post('/user_auth/login')
        .send(login_request);
}

export function login_user(login_request: *, _response: *): Promise<*> {
  return chai.request(webapp_server)
        .post('/user_auth/login')
        .send(login_request);
}

export function create_product(response: ExpressResponse): Promise<*>{
  const product_auth_token = response.body.response.token;
  const create_product_request = get_test_create_product_request();
  return chai.request(webapp_server)
        .post('/product')
        .set('Authorization', product_auth_token)
        .send(create_product_request);
}

export function update_product_address(response: ExpressResponse): Promise<*>{
  const update_auth_token = response.body.response.token;
  const update_product_address_request = get_test_create_product_request();
  const updated_product_address_request =  R.merge(update_product_address_request, {user_id: response.body.response.user_id});
  return chai.request(webapp_server)
        .post('/product/address/update')
        .set('Authorization', update_auth_token)
        .send(updated_product_address_request);
}

export function get_product_address(response: ExpressResponse): Promise<*>{
  const update_auth_token = response.body.response.token;
  const get_product_address_request = R.merge(InitialGetProductAddressWebRequest, {user_id: response.body.response.user_id});
  return chai.request(webapp_server)
        .get('/product/address')
        .set('Authorization', update_auth_token)
        .send(get_product_address_request);
}
