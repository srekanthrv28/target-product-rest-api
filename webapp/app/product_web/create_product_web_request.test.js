/* @flow */
'use strict';

import {it, describe} from 'mocha';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);
import * as test_helper from './product_web_api_test_helper.test';


const _create_product_request = describe("Create product request ", (): void => {
  const create_product_sucessfully = it("Create product sucessfully", (): Promise<*> => {
     return test_helper.login_admin(test_helper.get_admin_credentials())
    .then(test_helper.create_product)
    .catch((error: Error): Promise<*> => Promise.reject(error));
    });
    return create_product_sucessfully;
});
