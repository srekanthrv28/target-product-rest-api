/* @flow */
'use strict';

import {it, describe} from 'mocha';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);
import * as test_helper from './product_web_api_test_helper.test';
import * as R from 'ramda';

const _get_product_address_request = describe("Get product address request ", (): void => {
  const get_product_address_sucessfully = it("Get product address sucessfully", (): Promise<*> => {
     return test_helper.login_admin(test_helper.get_admin_credentials())
    .then(test_helper.create_product)
    .then(R.curry(test_helper.login_user)(test_helper.get_admin_credentials()))
    .then(test_helper.get_product_address)
    .catch((error: Error): Promise<*> => Promise.reject(error));
    });
    return get_product_address_sucessfully;
});
