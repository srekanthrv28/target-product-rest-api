/* @flow */
'use strict';

import {type $Request as ExpressRequest, type $Response as ExpressResponse} from 'express';
import * as web_response_wrapper from '../core_web/web_response_wrapper';
import * as request_wrapper from './product_web_request_wrapper';
import {product_boundary} from 'product';
import {product_price_boundary} from 'product_price';
import {get_product_db_datastore, get_product_price_db_datastore} from 'db';
import R from 'ramda';
//import {logger} from 'app_log';


export function create_product(request: ExpressRequest, response: ExpressResponse): Promise<*> {
   return request_wrapper.from_web_request_to_create_product_request(request)
    .then(R.curry(product_boundary.create_new_product)(get_product_db_datastore()))
    .then(R.curry(create_product_price)(request))
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

async function create_product_price(request: ExpressRequest, product_response: *): Promise<*>{
  const product_price_response = await request_wrapper.from_web_request_to_create_product_price_request(request, product_response)
                                .then(R.curry(product_price_boundary.create_product_price)(get_product_price_db_datastore()))
  return get_get_client_response(product_price_response, product_response);
}

export function update_product(request: ExpressRequest, response: ExpressResponse): Promise<*> {
  return request_wrapper.from_web_request_to_get_product_request(request)
    .then(R.curry(product_boundary.get_product_details)(get_product_db_datastore()))
    .then(R.curry(update_product_and_product_price)(request))
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

async function update_product_and_product_price(request: ExpressRequest, product_response: *): Promise<*>{
  const response =  await request_wrapper.from_web_request_to_update_product_request(request, product_response)
            .then(R.curry(product_boundary.update_product_details)(get_product_db_datastore()));

  const product_price = await request_wrapper.from_web_request_to_get_product_request(request)
            .then(R.curry(product_price_boundary.get_product_price)(get_product_price_db_datastore()));

  const product_price_response =  await request_wrapper.from_web_request_to_update_product_price_request(request, product_price)
            .then(R.curry(product_price_boundary.update_product_price)(get_product_price_db_datastore()));
  return get_get_client_response(product_price_response, response);
}

export function get_product(request: ExpressRequest, response: ExpressResponse): Promise<*> {
   return request_wrapper.from_web_request_to_get_product_request(request)
    .then(R.curry(product_boundary.get_product_details)(get_product_db_datastore()))
    .then(R.curry(get_product_price)(request))
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

async function get_product_price(request: ExpressRequest, product_response: *): Promise<*> {
  const product_price_response = await request_wrapper.from_web_request_to_get_product_request(request)
                                .then(R.curry(product_price_boundary.get_product_price)(get_product_price_db_datastore()))
  return get_get_client_response(product_price_response, product_response);
}

function get_get_client_response(product_price_response: *, product_response: *): Promise<*> {
  return {
     id: product_price_response.product_id,
     current_price: {
       value: product_price_response.value,
       currency_code: product_price_response.currency_code,
       product_dsc: product_response.description
     }
  }
}
