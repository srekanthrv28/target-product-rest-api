/* @flow */
'use strict';
import {type $Request as ExpressRequest} from 'express';
import R from 'ramda';
import {InitialCreateProductWebRequest} from './web_request/create_product_web_request';
import {InitialCreateProductPriceWebRequest} from './web_request/create_product_price_web_request';
import {InitialUpdateProductWebRequest} from './web_request/update_product_web_request'
import {GetProductAddressWebRequest, InitialGetProductAddressWebRequest} from './web_request/get_product_web_request';
import * as product_validator from './web_request/product_web_request_validator';


export function from_web_request_to_create_product_request(request: ExpressRequest): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductWebRequest), request.body);
  return product_validator.validate_create_product_request_async(web_request)
  .then(R.curry(send_successful_create_product_request)(request))
}

function send_successful_create_product_request(request: ExpressRequest, _response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductWebRequest), request.body);
  return Promise.resolve(web_request);
}

export function from_web_request_to_create_product_price_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductPriceWebRequest), request.body.current_price);
  const update_web_request = R.merge(web_request, {product_id: response.product_id});
  return product_validator.validate_create_product_price_request_async(update_web_request)
  .then(R.curry(send_successful_create_product_price_request)(request, response))
}

function send_successful_create_product_price_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductWebRequest), request.body.current_price);
  const update_web_request = R.merge(web_request, {product_id: response.product_id});
  return Promise.resolve(update_web_request);
}

export function from_web_request_to_get_product_request(request: ExpressRequest): Promise<*> {
  const web_request = R.pick(R.keys(InitialGetProductAddressWebRequest), request.params);
  return product_validator.validate_get_product_address_request_async(web_request)
  .then(R.curry(send_successful_get_product_request)(request))
}

function send_successful_get_product_request(request: ExpressRequest, _response: *): Promise<GetProductAddressWebRequest> {
  const web_request = R.pick(R.keys(InitialGetProductAddressWebRequest), request.params);
  return Promise.resolve(web_request);
}

export function from_web_request_to_update_product_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialUpdateProductWebRequest), request.body);
  const merge_request = R.merge(response, web_request);
  return product_validator.validate_create_product_request_async(merge_request)
  .then(R.curry(send_successful_update_product_request)(request, response))
}

function send_successful_update_product_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialUpdateProductWebRequest), request.body);
  const merge_request = R.merge(response, web_request);
  return Promise.resolve(merge_request);
}


export function from_web_request_to_update_product_price_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductPriceWebRequest), request.body.current_price);
  const update_web_request = R.merge(response, web_request);
  return product_validator.validate_create_product_price_request_async(update_web_request)
  .then(R.curry(send_successful_create_update_price_request)(request, response))
}

function send_successful_create_update_price_request(request: ExpressRequest, response: *): Promise<*> {
  const web_request = R.pick(R.keys(InitialCreateProductWebRequest), request.body.current_price);
  const update_web_request = R.merge(response, web_request);
  return Promise.resolve(update_web_request);
}
