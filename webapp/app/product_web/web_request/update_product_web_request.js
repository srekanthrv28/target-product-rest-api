/* @flow */
'use strict';

export interface UpdateProductWebRequest {
  +product_id: string,
  +name: string,
  +description: string
}

export const InitialUpdateProductWebRequest: UpdateProductWebRequest = {
  product_id: '',
  name: '',
  description: ''
};

export const UpdateProductWebRequestConstraints  = {
  product_id: {
    presence:true
  }
}
