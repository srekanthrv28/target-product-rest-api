/* @flow */
'use strict';

import {CreateProductWebRequest, CreateProductWebRequestConstraints} from './create_product_web_request';
import {CreateProductPriceWebRequest, CreateProductPriceWebRequestConstraints} from './create_product_price_web_request';
import {UpdateProductAddressWebRequest, UpdateProductAddressWebRequestConstraints} from './update_product_web_request';
import {GetProductAddressWebRequest, GetProductAddressWebRequestConstraints} from './get_product_web_request';
import validate from 'validate.js';
import {InitialAppError} from 'tiny_error';
import R from 'ramda';

export function validate_create_product_request_async(create_product_web_request: CreateProductWebRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_product_web_request, CreateProductWebRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_product_web_request);
  }
  return send_error(validation_result);
}

export function validate_create_product_price_request_async(create_product_price_web_request: CreateProductPriceWebRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_product_price_web_request, CreateProductPriceWebRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_product_price_web_request);
  }
  return send_error(validation_result);
}

function send_error(validation_result: mixed): Promise<*>  {
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}

export function validate_update_product_request_async(update_product_address_web_request: UpdateProductAddressWebRequest): Promise<*> {
  const validation_result: mixed = validate.validate(update_product_address_web_request, UpdateProductAddressWebRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(update_product_address_web_request);
  }
  return send_error(validation_result);
}

export function validate_get_product_address_request_async(get_product_address_web_request: GetProductAddressWebRequest): Promise<*> {
  const validation_result: mixed = validate.validate(get_product_address_web_request, GetProductAddressWebRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(get_product_address_web_request);
  }
  return send_error(validation_result);
}
