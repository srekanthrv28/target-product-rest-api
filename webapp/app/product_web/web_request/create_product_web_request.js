/* @flow */
'use strict';

export interface CreateProductWebRequest {
  +name: string,
  +description: string
}

export const InitialCreateProductWebRequest: CreateProductWebRequest = {
  name: '',
  description: ''
};

export const CreateProductWebRequestConstraints  = {
  name: {
    presence: true
  },
  description: {
    presence: true
  }
}
