/* @flow */
'use strict';

export interface GetProductAddressWebRequest {
  +product_id: string
}

export const InitialGetProductAddressWebRequest: GetProductAddressWebRequest = {
  product_id: '',
};

export const GetProductAddressWebRequestConstraints  = {
  product_id: {
    presence:true
  }
}
