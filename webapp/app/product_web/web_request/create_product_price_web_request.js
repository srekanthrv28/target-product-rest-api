/* @flow */
'use strict';

export interface CreateProductPriceWebRequest {
  +value: number,
  +currency_code: string
}

export const InitialCreateProductPriceWebRequest: CreateProductPriceWebRequest = {
  value: '',
  currency_code: ''
};

export const CreateProductPriceWebRequestConstraints  = {
  value: {
    presence: true
  },
  currency_code: {
    presence: true
  }
}
