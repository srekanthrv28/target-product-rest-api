/* @flow */
'use strict';

import {router as user_router} from '../user_profile_web/user_router';
import {router as user_auth_router} from '../user_auth_web/user_auth_router';
import * as web_response_wrapper from '../core_web/web_response_wrapper';
import {InitialAppError} from 'tiny_error';
import {router as health_check_router} from '../health_check_web/health_check_router';
import {router as product_router} from '../product_web/product_router';


import type {
  $Application,
  Router,
  $Request as ExpressRequest,
  $Response as ExpressResponse
} from 'express';
import R from 'ramda'

export function add_all_web_app_routes(app: $Application): number {
  const _user = app.use('/user', user_router);
  const _user_auth_router = app.use('/user_auth', user_auth_router);
  const _health_check_router = app.use('/api/health_check', health_check_router);
  const _product_router = app.use('/product', product_router);
  /* keep this as last error*/
  const _default_error = add_default_error_routes(app)
  return 0;
}

function add_default_error_routes(app: Router): number {
  const _x = app.all('*', (request: ExpressRequest, response: ExpressResponse): ExpressResponse => {
    const _y = web_response_wrapper.send_failed_response(response, R.merge(InitialAppError, {reason: `Requested resource does not exists`, type: "BAD_REQUEST"}));
    return response;
  });
  return 0;
}
