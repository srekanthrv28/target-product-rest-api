/* @flow */
'use strict';
import {type $Request as ExpressRequest} from 'express';
import {
  InitialCreateUserRequest
} from 'user';
import R from 'ramda'
import {InitialCreateUserWebRequest, type CreateUserWebRequest} from './web_request/create_user_web_request';
import * as validator from './web_request/user_web_request_validator';


export function from_web_request_to_create_user_request(request: ExpressRequest): Promise<*> {
  const web_request: CreateUserWebRequest = R.pick(R.keys(InitialCreateUserWebRequest), request.body);
  return validator.validate_create_user_request_async(web_request)
           .then(R.curry(send_successful_create_user_request)(request));
}

function send_successful_create_user_request(request: ExpressRequest, _res: *): Promise<*> {
  return Promise.resolve(R.pick(R.keys(InitialCreateUserRequest), request.body));
}
