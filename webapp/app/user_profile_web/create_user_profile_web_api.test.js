/* @flow */
'use strict';

import {it, describe} from 'mocha';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);
import * as test_helper from './user_profile_web_api_test_helper.test';


const _create_user = describe("Create User Profile Test", (): void => {
  const create_user_successfully = it("Create a new User successfully", (): Promise<> => {
    return test_helper.create_test_user_profile()
      .then(test_helper.validate_user_profile_success_response)
      .catch((error: Error): Promise<> => Promise.reject(error));
  });
  return create_user_successfully;
});
