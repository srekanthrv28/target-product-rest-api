/* @flow */
'use strict';


import {user_boundary} from 'user';
import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';
import * as request_wrapper from './user_profile_request_wrapper';
import * as web_response_wrapper from '../core_web/web_response_wrapper';
import {get_user_db_datastore} from 'db';
import R from 'ramda';


export function create_user_profile(request: ExpressRequest, response: ExpressResponse): Promise<*> {
   return request_wrapper.from_web_request_to_create_user_request(request)
    .then(R.curry(user_boundary.create_new_user)(get_user_db_datastore()))
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}
