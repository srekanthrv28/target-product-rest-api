/* @flow */
'use strict';

import faker from 'faker';
import {type $Response as ExpressResponse} from 'express';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
import webapp_server from '../core_web/webapp_server';
import {CreateUserWebRequest} from './web_request/create_user_web_request';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);

export interface UserTokenResponse {
  user_id: string,
  token: string
}

export function get_test_create_user_profile_request(): CreateUserWebRequest {
  //const fake_pass: string = faker.internet.password();
  const test_create_user_request = {
    email: faker.internet.email(),
    password: 'target@123',
    confirm_password: 'target@123'
  }
  return test_create_user_request;
}

export function validate_user_profile_success_response(response: ExpressResponse): * {
  const _validated_response = validate_success_response(response);
  return response.body.response;
}

export function create_test_user_profile(): Promise<> {
  const create_user_request = get_test_create_user_profile_request();
  return chai.request(webapp_server)
        .post('/user/profile')
        .send(create_user_request);
}

export function create_and_validate_user(_number: number): Promise<> {
  return create_test_user_profile().then(validate_user_profile_success_response);
}

export function validate_success_response(response: ExpressResponse): ExpressResponse {
  const _status = response.should.have.status(200);
  const _body_type = response.body.should.be.a('object');
  const _success = response.body.should.have.property('success').eql(true);
  const _errors = response.body.should.not.have.property('errors');
  const _body_resp = response.body.should.have.property('response');
  return response;
}
