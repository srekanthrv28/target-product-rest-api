/* @flow */
'use strict';

import {
  CreateUserWebRequest,
  CreateUserWebRequestConstraints
} from './create_user_web_request';

import validate from 'validate.js';
import {InitialAppError} from 'tiny_error';
import R from 'ramda';

export function validate_create_user_request_async(create_user_web_request: CreateUserWebRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_user_web_request, CreateUserWebRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_user_web_request);
  }
  return send_error(validation_result);
}

function send_error(validation_result: mixed): Promise<*>  {
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}
