/* @flow */
'use strict';

export interface CreateUserWebRequest {
  +email: string,
  +password: string,
  +confirm_password: string
}

export const InitialCreateUserWebRequest: CreateUserWebRequest = {
  email: '',
  password: '',
  confirm_password: ''
};

export const CreateUserWebRequestConstraints  = {
  email: {
    presence: true
  },
  password: {
    presence: true
  },
  confirm_password: {
    equality: "password"
  }
}
