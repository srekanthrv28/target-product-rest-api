/* @flow */
'use strict';
import * as express from 'express';
import type {$Request, $Response} from 'express';
import * as user_profile_controller from './user_profile_controller';
const router: express.Router = express.Router();

/**
 * @swagger
 * /user/profile:
 *   post:
 *     tags:
 *       - User Profile
 *     description: Create User Profile
 *     parameters:
 *       - name: user
 *         description: User Profile object
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Create User Profile
 */
const _y = router.post('/profile', (req: $Request, res: $Response): number => {
  const _updated_response = user_profile_controller.create_user_profile(req, res);
  return 0;
});


export {
  router
};
