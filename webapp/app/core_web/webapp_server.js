/* @flow */
'use strict';

import * as webapp_routes from '../routes_web/webapp_router';
import express from 'express';
import {logger} from 'app_log';
import body_parser from 'body-parser';
import * as cors_spec from './cors_spec';
import {define_all_models, is_db_enabled} from 'db';
import R from 'ramda';
import * as swagger_spec from './swagger_spec';
import * as logger_spec from './logger_spec';

const __public_dir = __dirname + '/../../public';

const port = process.env.PORT || 3000;

const webapp_server = express();


if (is_swagger_enabled() == true) {
  const _swagger_setup = swagger_spec.setup_swagger(webapp_server);
}

const _cors_setup = cors_spec.setup_cors(webapp_server);

const _a = webapp_server.use(body_parser.json());

const _x = webapp_server.use(express.static(__public_dir));

const _request_logger_setup = logger_spec.setup_web_request_logging(webapp_server);

const _y = webapp_routes.add_all_web_app_routes(webapp_server);

const _error_logger_setup = logger_spec.setup_error_logging(webapp_server);

function connect_to_db_and_start_server(): Promise<> {
  const _log_db_enabled = logger.info("Connecting to the database...");
  return define_all_models()
    .then(R.curry(start_server))
    .catch((error: mixed): mixed  => {
      const _log_error = logger.error("Error while connecting to the database server", error);
      return error;
    });
}

function start_server(): number {
  const _listen_result = webapp_server.listen(port, (): number => {
    const _x = logger.info('Target Rest API Server Listening on port', port);
    return 0;
  });
  return 0;
}


function is_swagger_enabled(): boolean {
  return R.equals('true')(R.toLower(R.defaultTo('false', process.env.SWAGGER_ENABLED)));
}

if (is_db_enabled() == true) {
  const _db_enabled_server = connect_to_db_and_start_server();
}
else {
  const _server_without_db = start_server();
}

export default webapp_server;
