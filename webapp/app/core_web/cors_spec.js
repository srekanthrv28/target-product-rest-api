/* @flow */
'use strict';

import type {
  $Application,
  $Request as ExpressRequest,
  $Response as ExpressResponse
} from 'express';


export function setup_cors(webapp_server: $Application): $Application {
  return webapp_server.use((req: ExpressRequest , res: ExpressResponse, next: () => mixed): mixed => {
    const _h1 = res.header("Access-Control-Allow-Origin", "*");
    const _h2 = res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    return next();
  });
}
