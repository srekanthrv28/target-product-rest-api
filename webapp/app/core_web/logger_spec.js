/* @flow */
'use strict';

import express_bunyan_logger from 'express-bunyan-logger';
import type {$Application} from 'express';
import config from 'config';
import R from 'ramda';

const node_env = R.defaultTo("development", process.env.NODE_ENV)
const log_config = config.get(R.join('.', [node_env, 'LogConfig']));

const format_options = {
                        format: ":remote-address",
                        level: log_config.log_level,
                        name: log_config.app_name,
                        obfuscate: ['user-agent','req-headers', 'res-headers', 'req.headers']
                      }

export function setup_web_request_logging(webapp_server: $Application): $Application {
  return webapp_server.use(express_bunyan_logger(format_options));
}

export function setup_error_logging(webapp_server: $Application): $Application {
  return webapp_server.use(express_bunyan_logger.errorLogger(format_options));
}
