/* @flow */
'use strict';

import {WebResponse} from './web_response';
import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';
import {logger} from 'app_log';
import R from 'ramda';
import {
  InitialAppError
} from 'tiny_error';

export function send_success_response<T>(request: ExpressRequest, response: ExpressResponse, api_response: T): ExpressResponse {
  const success_response = build_successful_response(api_response);
  const _x = logger.info("Sending success response....", R.toString(success_response));
  return response.status(200).send(success_response);
}

export function send_failed_response(response: ExpressResponse, error: InitialAppError | Array | string): ExpressResponse {
  const failed_resp = build_failed_response(error);
  const _x = logger.info("Sending failed response....", R.toString(failed_resp));
  return response.send(failed_resp);
}

function build_successful_response<T>(response: T): WebResponse<T> {
  const web_response: WebResponse<T> = {
    success: true,
    response: response
  }
  return web_response;
}

function build_failed_response<T>(input_errors: string | InitialAppError | Array): WebResponse<T> {
  const errors = build_error_messages(input_errors);
  const web_response: WebResponse<T> = {
    success: false,
    errors: errors
  }
  return web_response;
}

function build_error_messages(errors: string | InitialAppError | Array): Array<string> {
  if (typeof errors == InitialAppError) {
    return [errors];
  } else if (typeof errors == 'string') {
    return [errors];
  }
  return [errors];
}
