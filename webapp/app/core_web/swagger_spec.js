/* @flow */
'use strict';

import swagger_jsdoc from 'swagger-jsdoc';
import type {
  $Application,
  $Request as ExpressRequest,
  $Response as ExpressResponse
} from 'express';


export function setup_swagger(webapp_server: $Application): $Application {
  const swagger_definition = {
    info: {
      title: 'Target Rest API REST API',
      version: '0.1.0',
      description: 'Target Rest API REST API',
    },
    host: 'localhost:3000',
    basePath: '/',
  };

  const options = {
    swaggerDefinition: swagger_definition,
    apis: ['../**/*router.js'],
  };

  const swagger_spec = swagger_jsdoc(options);

  // serve swagger
  const _resp = webapp_server.get('/swagger.json', (req: ExpressRequest, res: ExpressResponse): ExpressResponse => {
    const _header = res.setHeader('Content-Type', 'application/json');
    const _sent_resp = res.send(swagger_spec);
    return res;
  });
  return webapp_server;
}
