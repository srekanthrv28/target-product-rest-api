/* @flow */
'use strict';

import {user_authentication_boundary, AuthenticatedUserResponse} from 'user';
import {
  type $Request as ExpressRequest,
  type $Response as ExpressResponse
} from 'express';
import * as request_wrapper from './user_auth_request_wrapper';
import * as web_response_wrapper from '../core_web/web_response_wrapper';
import {get_user_db_datastore, get_user_auth_token_db_datastore} from 'db';
import R from 'ramda';
import {user_auth_token_boundary, InitialCreateUserAuthTokenRequest, InitialRevokeUserAuthTokenRequest} from 'user_auth_store';
const token_datastore = get_user_auth_token_db_datastore();
const user_datastore = get_user_db_datastore();
//import {logger} from 'app_log';

export function authenticate_user(request: ExpressRequest, response: ExpressResponse): Promise<> {
  return user_authentication_boundary.authenticate_login_user(user_datastore, request_wrapper.from_web_request_to_authenticate_user_request(request))
    .then(add_token_for_user)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}

export function logout_user(request: ExpressRequest, response: ExpressResponse): Promise<> {
  const revoke_token_request = R.merge(InitialRevokeUserAuthTokenRequest, {
    token: request.user.token
  });
  return user_auth_token_boundary.revoke_user_auth_token(token_datastore, revoke_token_request)
    .then(R.curry(web_response_wrapper.send_success_response)(request, response))
    .catch(R.curry(web_response_wrapper.send_failed_response)(response));
}


export function add_token_for_user(user_auth_response: AuthenticatedUserResponse): Promise<> {
  const create_auth_token_req = R.merge(InitialCreateUserAuthTokenRequest, {
    user_id: user_auth_response.user_id,
    permissions: [],
  });
  return user_auth_token_boundary.create_user_auth_token(token_datastore, create_auth_token_req);
}
