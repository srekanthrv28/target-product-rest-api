/* @flow */
'use strict';

import passport from 'passport';
// import {get_user_auth_store_db_datastore} from 'db';
import config from 'config';
import passport_jwt from 'passport-jwt';
const jwt_strategy = passport_jwt.Strategy;
const extract_jwt = passport_jwt.ExtractJwt;
import R from 'ramda';
import {InitialVerifyUserAuthTokenRequest, user_auth_token_boundary, VerifiedUserAuthTokenResponse} from 'user_auth_store';
import {user_boundary, InitialGetUserProfileRequest} from 'user';
import {logger} from 'app_log';
import {AppError} from 'tiny_error';
import {get_user_auth_token_db_datastore, get_user_db_datastore} from 'db';

const token_datastore = get_user_auth_token_db_datastore();

interface JwtToken {
  sub: string,
  jti: string,
  iat: string,
  secret: string
}

const node_env = R.defaultTo("development", process.env.NODE_ENV)
export const passport_secret = config.get(R.join('.', [node_env, 'Authentication.JwtTokenSecret']));

// Setup options for JWT Strategy
const jwt_options = {
  jwtFromRequest: extract_jwt.fromHeader('authorization'),
  secretOrKey: passport_secret
};

// Create JWT strategy
const jwt_login = new jwt_strategy(jwt_options, async function(payload: JwtToken, done: (error: Error, auth_user_resp: mixed) => void): Promise<> {
  const verify_auth_token_req = R.merge(InitialVerifyUserAuthTokenRequest, {token: payload.jti})
  return user_auth_token_boundary.verify_user_auth_token(token_datastore, verify_auth_token_req)
    .then(async (verified_token_resp: VerifiedUserAuthTokenResponse): void => {
      const get_user_profile_request = R.merge(InitialGetUserProfileRequest, {user_id: verified_token_resp.user_id});
      const user_details = await user_boundary.find_user_by_id(get_user_db_datastore(), get_user_profile_request);
      if(user_details.status === 'inactive') {
        const _log_error = logger.error("Invalid Authentication Token: ", R.toString(payload));
        return done(null, false); //eslint-disable-line fp/no-nil
      }
      if (R.or(R.isNil(verified_token_resp), !R.propEq('token_verified', true, verified_token_resp))) {
        const _log_error = logger.error("Invalid Authentication Token: ", R.toString(payload));
        return done(null, false); //eslint-disable-line fp/no-nil
      }
      const _log_success = logger.info("Successfully validated the token: ", R.toString(payload.jti));
      const authenticated_user = {
        id: verified_token_resp.user_id,
        role: verified_token_resp.role,
        permissions: verified_token_resp.permissions,
        token: payload.jti
      };
      return done(null, authenticated_user); //eslint-disable-line fp/no-nil
    })
    .catch((app_error: AppError): void => {
      const _log_err = logger.error("Error validating the token. Error: ", R.toString(app_error));
      return done(null, false); //eslint-disable-line fp/no-nil
    });
});

const _use_jwt_login = passport.use(jwt_login);
export const require_auth = passport.authenticate('jwt', {session:false});

// function handle_verification_error(done: (error: Error, auth_user_resp: mixed) => void, app_error: AppError): void {
//   const _log_err = logger.error("Error validating the token. Error: ", R.toString(app_error));
//   return done(new Error(app_error.reason), false); //eslint-disable-line fp/no-nil
// }
