/* @flow */
'use strict';

import {it, describe} from 'mocha';

import {type $Response as ExpressResponse} from 'express';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
import chai_http from 'chai-http';
import webapp_server from '../core_web/webapp_server';
import config from 'config';

const node_env = R.defaultTo("development", process.env.NODE_ENV)
export const jwt_secret = config.get(R.join('.', [node_env, 'Authentication.JwtTokenSecret']));
import {CreateUserWebRequest} from '../user_profile_web/web_request/create_user_web_request';

const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
const _chai_http = chai.use(chai_http);
import R from 'ramda';
import {
  get_test_create_user_profile_request,
  validate_success_response,
  validate_user_profile_success_response
} from '../user_profile_web/user_profile_web_api_test_helper.test';


export function create_test_user_profile(create_user_request: CreateUserWebRequest): Promise<> {
  return chai.request(webapp_server)
        .post('/user/profile')
        .send(create_user_request);
}

export function login_user(email: string, password: string): Promise<> {
  const login_request = {email: email, password: password};
  return chai.request(webapp_server)
        .post('/user_auth/login')
        .send(login_request);
}

const _get_user_profile_after_logout = describe("Get User Profile after logout", (): void => {
  const assign_shift_successfully = it("User Profile Get should fail after logout", (): Promise<> => {
    const test_create_user_request = get_test_create_user_profile_request();
    return create_test_user_profile(test_create_user_request)
      .then(validate_user_profile_success_response)
      .then(R.curry(login_and_get_user_profile)(test_create_user_request))
      .catch((error: Error): Promise<> => Promise.reject(error));
  });
  return assign_shift_successfully;
});

function login_and_get_user_profile(create_user_request: CreateUserWebRequest, _user_profile: UserProfileResponse): Promise<> {
  return login_user(create_user_request.email, create_user_request.password)
    .then(R.curry(validate_login_response)(create_user_request));
}

function validate_login_response(create_user_request: CreateUserWebRequest, response: ExpressResponse): ExpressResponse {
  const _success_response = validate_success_response(response)
  const _check_response = response.body.should.have.property('response');
  const login_response = response.body.response;
  const _check_token = login_response.should.have.property('token')
  const _check_user_id = login_response.should.have.property('user_id');
  return response;
}
