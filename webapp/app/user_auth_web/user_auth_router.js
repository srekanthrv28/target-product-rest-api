/* @flow */
'use strict';
import * as express from 'express';
import type {$Request, $Response} from 'express';
import * as user_auth_controller from './user_auth_controller';
const router: express.Router = express.Router();
import {require_auth} from './user_auth_passport_service';
/**
 * @swagger
 * /user_auth/login:
 *   post:
 *     tags:
 *       - User Authentication
 *     description: Authenticate User
 *     parameters:
 *       - name: user
 *         description: Authenticate User Object
 *         in: body
 *         required: true
 *         schema:
 *           required: true
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Authenticate User
 */
const _auth_login = router.post('/login', (req: $Request, res: $Response): number => {
  const _updated_response = user_auth_controller.authenticate_user(req, res);
  return 0;
});

/**
 * @swagger
 * /user_auth/logout:
 *   post:
 *     tags:
 *       - User Authentication
 *     description: Logout User
 *     parameters:
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Logout User
 */
const _y = router.post('/logout', require_auth, (req: $Request, res: $Response): number => {
  const _updated_response = user_auth_controller.logout_user(req, res);
  return 0;
});


export {
  router
};
