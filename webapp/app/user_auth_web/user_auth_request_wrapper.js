/* @flow */
'use strict';
import {type $Request as ExpressRequest} from 'express';

import {
  InitialAuthenticateUserRequest,
  type AuthenticateUserRequest,
  InitialFindUserRequest
} from 'user';

import R from 'ramda'

export function from_web_request_to_authenticate_user_request(request: ExpressRequest): AuthenticateUserRequest {
  return R.pick(R.keys(InitialAuthenticateUserRequest), request.body);
}

export function from_web_request_to_get_user_request(request: ExpressRequest): GetUserRequest {
  return R.pick(R.keys(InitialFindUserRequest), {user_id: request.user.id});
}
