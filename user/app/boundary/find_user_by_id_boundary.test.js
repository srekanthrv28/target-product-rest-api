/* @flow */
'use strict';

import {it} from 'mocha';
import * as user_boundary from './user_boundary';
import {CreateUserRequest} from '../request/create_user_request';
import {InitialFindUserRequest} from '../request/find_user_request';
import {UserResponse} from '../response/user_response';
import * as faker from 'faker';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import * as in_memory_datastore from '../datastore/in_memory_user_datastore';

const get_create_user_request = (): CreateUserRequest => {
  const pass = faker.internet.password();
  return {
    email: faker.internet.email(),
    password: pass+'@',
    confirm_password: pass+'@'
  }
};

const _get_user_by_id_test = it("Should create user and get user by id", (): Promise<> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const create_user_promise: Promise<> = user_boundary.create_new_user(in_memory_datastore, test_create_request);
  const get_user_promise = create_user_promise.then((user_response: UserResponse): Promise<> => {
    const find_user_req = R.merge(InitialFindUserRequest, {user_id: user_response.user_id});
    return user_boundary.find_user_by_id(in_memory_datastore, find_user_req);
  });
  return Promise.all([
    get_user_promise.should.eventually.be.fulfilled,
    get_user_promise.should.eventually.have.property('user_id'),
    get_user_promise.should.eventually.have.property('email').to.equal(test_create_request.email),
  ]);
});
