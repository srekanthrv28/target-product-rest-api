/* @flow */
'use strict';

import {
  it
} from 'mocha';
import * as user_boundary from './user_boundary';
import {
  CreateUserRequest
} from '../request/create_user_request';
import * as faker from 'faker';

import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import {logger} from 'app_log';
import * as in_memory_datastore from '../datastore/in_memory_user_datastore';

const get_create_user_request = (): CreateUserRequest => {
  const pass = faker.internet.password();
  return {
    email: faker.internet.email(),
    password: pass+'@',
    confirm_password: pass+'@'
  }
};

const _create_new_user = it("Should create user with valid create user request", (): Promise<*> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const create_user_promise: Promise<*> = user_boundary.create_new_user(in_memory_datastore, test_create_request);
  return Promise.all([
    create_user_promise.should.eventually.be.fulfilled,
    create_user_promise.should.eventually.have.property('user_id'),
    create_user_promise.should.eventually.have.property('email'),
  ]);
});

const _dont_create_duplicate_user = it("Should not create user with duplicate email id", (): Promise<*> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const create_user_promise: Promise<*> = user_boundary.create_new_user(in_memory_datastore, test_create_request);
  const create_dup_user_promise = user_boundary.create_new_user(in_memory_datastore, test_create_request);
  const _a = logger.info("_dont_create_duplicate_user: ", create_dup_user_promise);
  return Promise.all([
    create_user_promise.should.eventually.be.fulfilled,
    create_user_promise.should.eventually.have.property('user_id'),
    create_user_promise.should.eventually.have.property('email'),
    create_dup_user_promise.should.eventually.be.rejected
  ]);
});


const _dont_create_user = it("Should not create user with non-matching passwords", (): Promise<*> => {
  const test_create_request: CreateUserRequest = get_create_user_request();
  const test_create_invalid_request = R.assoc('confirm_password', faker.internet.password(), test_create_request);
  const create_user_promise: Promise<*> = user_boundary.create_new_user(in_memory_datastore, test_create_invalid_request);
  return Promise.all([
    create_user_promise.should.eventually.be.rejected
  ]);
});
