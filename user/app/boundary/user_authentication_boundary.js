/* @flow */
'use strict';

import {authenticate_user} from '../interactor/authenticate_user_interactor';
import {AuthenticateUserRequest} from '../request/authenticate_user_request';
import {type UserDatastoreProvider} from '../datastore/user_datastore_provider';

export function authenticate_login_user(datastore: UserDatastoreProvider, authenticatate_user_request: AuthenticateUserRequest): Promise<*> {
  return authenticate_user(datastore, authenticatate_user_request);
}
