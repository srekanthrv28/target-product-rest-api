/* @flow */
'use strict';

import {it} from 'mocha';
import * as user_boundary from './user_boundary';
import * as user_authentication_boundary from './user_authentication_boundary';
import {CreateUserRequest} from '../request/create_user_request';
import {AuthenticateUserRequest} from '../request/authenticate_user_request';
import * as faker from 'faker';
import 'babel-polyfill';
import chai from 'chai';
import chai_as_promised from 'chai-as-promised';
const _chai_should = chai.should();
const _chai_used = chai.use(chai_as_promised);
import R from 'ramda';
import * as in_memory_datastore from '../datastore/in_memory_user_datastore';


const get_create_user_request = (): CreateUserRequest => {
  const pass = faker.internet.password();
  return {
    email: faker.internet.email(),
    password: pass+'@',
    confirm_password: pass+'@'
  }
};

const get_user_for_authenticate_request = (user_user_request: CreateUserRequest): AuthenticateUserRequest => {
  return {
    email: user_user_request.email,
    password: user_user_request.password
  }
};

const _get_authenticated_user= it('Should authenticate user with valid credentials request', (): Promise<> => {
   const test_create_request: CreateUserRequest = get_create_user_request();
   return user_boundary.create_new_user(in_memory_datastore, test_create_request)
    .then(R.curry(get_authenticated_user_response)(test_create_request));
});

export function get_authenticated_user_response(request: CreateUserRequest, create_user: CreateUserRequest): Promise<> {
  const test_request = get_user_for_authenticate_request(request);
  const _test = create_user;
  const get_authenticated_user_promise = user_authentication_boundary.authenticate_login_user(in_memory_datastore, test_request);
  return Promise.all([
    get_authenticated_user_promise.should.eventually.be.fulfilled,
    get_authenticated_user_promise.should.eventually.have.property('user_id')
  ]);
}
