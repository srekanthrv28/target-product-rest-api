/* @flow */
'use strict';

import {create_user} from '../interactor/create_user_interactor';
import * as find_user_interactor from '../interactor/find_user_interactor';
import {type UserDatastoreProvider} from '../datastore/user_datastore_provider';
import {CreateUserRequest} from '../request/create_user_request';
import {FindUserRequest} from '../request/find_user_request';

export function create_new_user(datastore: UserDatastoreProvider, create_user_request: CreateUserRequest): Promise<*> {
  return create_user(datastore, create_user_request);
}

export function find_user_by_id(datastore: UserDatastoreProvider, find_user_request: FindUserRequest): Promise<*> {
  return find_user_interactor.find_user_by_id(datastore, find_user_request);
}
