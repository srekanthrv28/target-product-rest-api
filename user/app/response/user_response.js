/* @flow */
'use strict';

export interface UserResponse {
  +email: string,
  +user_id: string,
  +status: string
}

export const InitialUserResponse: UserResponse = {
  email: '',
  user_id: '',
  status: ''
};
