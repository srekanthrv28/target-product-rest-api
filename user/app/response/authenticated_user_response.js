/* @flow */
'use strict';

export interface AuthenticatedUserResponse {
  +user_id: string,
  +user_role: string,
  +email_verified: boolean,
  +mobile_verified: boolean,
  +status: string
}

export const InitialAuthenticatedUserResponse: AuthenticatedUserResponse = {
  user_id: '',
  user_role: '',
  email_verified: false,
  mobile_verified: false,
  status:''
};
