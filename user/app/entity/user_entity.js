/* @flow */
'use strict';


export interface UserEntity {
  +id?: string,
  +email: string,
  +password?: string
}

export const InitialUserEntity = {
  id: '',
  email: '',
  password: '',
};
