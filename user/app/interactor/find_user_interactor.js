/* @flow */
'use strict';

import {logger} from 'app_log';

import {FindUserRequest} from '../request/find_user_request';
import {UserResponse} from '../response/user_response';
import {UserEntity} from '../entity/user_entity';
import * as request_validator from '../request/user_request_validator';
import * as user_entity_wrapper from './user_entity_wrapper';
import * as user_datastore from '../datastore/user_datastore';
import {type UserDatastoreProvider} from '../datastore/user_datastore_provider';
import R from 'ramda';

export function find_user_by_id(datastore: UserDatastoreProvider, request: FindUserRequest): Promise<> {
  const _x = logger.info(`Finding user with request: ${R.toString(request)}`);
  return request_validator.validate_find_user_request_async(request)
    .then(user_entity_wrapper.from_find_user_request_to_entity)
    .then(R.curry(user_datastore.find_datastore_user_by_user_id)(datastore))
    .then(user_fetch_success);
}

function user_fetch_success(user_entity: UserEntity): UserResponse {
  const _x = logger.info(`Successfully found user: ${JSON.stringify(user_entity)}`);
  return user_entity_wrapper.from_user_entity_to_response(user_entity);
}
