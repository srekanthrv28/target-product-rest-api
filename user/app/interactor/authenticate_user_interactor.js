/* @flow */
'use strict';

import {logger} from 'app_log';

import {AuthenticateUserRequest} from '../request/authenticate_user_request';
import {AuthenticatedUserResponse} from '../response/authenticated_user_response';
import {UserEntity} from '../entity/user_entity';
import * as user_datastore from '../datastore/user_datastore';
import * as user_entity_wrapper from './user_entity_wrapper';
import * as request_validator from '../request/authenticate_user_request_validator';
import {type UserDatastoreProvider} from '../datastore/user_datastore_provider';
import R from 'ramda';
import {InitialAppError} from 'tiny_error';
import bcrypt from 'bcrypt';

export function authenticate_user(datastore: UserDatastoreProvider, request: AuthenticateUserRequest): Promise<*> {
  const _x = logger.info(`Authenticating user with request: ${R.toString(request)}`);
  return request_validator.validate_request_async(request)
    .then(R.curry(user_datastore.find_datastore_user_by_email)(datastore))
    .then(R.curry(validate_user)(request));
}

function validate_user(request: AuthenticateUserRequest, user_entity: UserEntity): Promise<AuthenticatedUserResponse | Error> {
  if (R.isNil(user_entity)) {
    return Promise.reject(R.merge(InitialAppError, {reason: `The email you have entered doesn't match an account`, type: "UNAUTHORIZED_REQUEST"}));
  }
  else if (R.isNil(user_entity.password)) {
    return Promise.reject(R.merge(InitialAppError, {reason: `The email you have entered doesn't have login access.`, type: "UNAUTHORIZED_REQUEST"}));
  }
  const _x = logger.info("Validating the user data: ", R.toString(user_entity));
  return validate_and_get_user_id(user_entity, request)
}

function validate_and_get_user_id(user_entity: UserEntity, request: AuthenticateUserRequest): Promise<AuthenticatedUserResponse | Error> {
  return R.ifElse(R.curry(validate_credentials), get_user_data, error_invalid_credentails)(request, user_entity);
}

function get_user_data(request: AuthenticateUserRequest,  user_entity: UserEntity): AuthenticatedUserResponse {
  return user_entity_wrapper.from_user_entity_to_get_validated_response(user_entity)
}

function validate_credentials(request: AuthenticateUserRequest,  user_entity: UserEntity): boolean {
  const request_with_lowercase_email = R.merge(request, {email: R.toLower(request.email)});
  return R.ifElse(R.curry(validate_email), validate_password, R.F)(request_with_lowercase_email, user_entity);
}

function validate_email(request: AuthenticateUserRequest,  user_entity: UserEntity): boolean {
  return R.equals(R.pick("email", user_entity), R.pick("email", request))
}

function validate_password(request: AuthenticateUserRequest,  user_entity: UserEntity): boolean {
  return R.curry(compare_password)(R.prop("password", request), R.prop("password", user_entity));
}

function compare_password(password: string, password_hash: string): boolean {
  return bcrypt.compareSync(password, password_hash);
}

function error_invalid_credentails(): Promise<Error> {
  const _a = logger.error(`The password you have entered is incorrect.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `The password you have entered is incorrect`, type: "UNAUTHORIZED_REQUEST"}));
}
