/* @flow */
'use strict';

import {CreateUserRequest} from '../request/create_user_request';
import {FindUserRequest} from '../request/find_user_request';
import {InitialUserResponse, type UserResponse} from '../response/user_response';
import {InitialUserEntity, type UserEntity} from '../entity/user_entity';
import {InitialAuthenticatedUserResponse, AuthenticatedUserResponse} from "../response/authenticated_user_response";

import R from 'ramda';

export function from_user_entity_to_response(user_entity: UserEntity): UserResponse {
  const response_with_out_id = R.pick(R.keys(InitialUserResponse), user_entity);
  return R.merge(response_with_out_id, {user_id: user_entity.id});
}

export function from_user_request_to_entity(request: CreateUserRequest): UserEntity {
  return R.pick(R.keys(InitialUserEntity), request);
}

export function from_find_user_request_to_entity(request: FindUserRequest): UserEntity {
  return R.merge(InitialUserEntity, {id: request.user_id});
}

export function from_user_entity_to_get_validated_response(user_entity: UserEntity): AuthenticatedUserResponse {
  const response_with_out_id = R.pick(R.keys(InitialAuthenticatedUserResponse), user_entity);
  return R.merge(response_with_out_id, {user_id: user_entity.id});
}
