/* @flow */
'use strict';

import {logger} from 'app_log';

import {
  CreateUserRequest
} from '../request/create_user_request';
import {UserResponse} from '../response/user_response';
import {
  create_datastore_user
} from '../datastore/user_datastore';
import {
  UserEntity
} from '../entity/user_entity';
import * as request_validator from '../request/user_request_validator';
import * as user_entity_wrapper from './user_entity_wrapper';
import {type UserDatastoreProvider} from '../datastore/user_datastore_provider';
import R from 'ramda';

export function create_user(datastore: UserDatastoreProvider, request: CreateUserRequest): Promise<*> {
  const _x = logger.info(`Creating user with request: ${R.toString(request)}`);
  return request_validator.validate_create_user_request_async(request)
    .then(user_entity_wrapper.from_user_request_to_entity)
    .then(R.curry(create_datastore_user)(datastore))
    .then(user_creation_success);
}

function user_creation_success(user_entity: UserEntity): UserResponse {
  const _x = logger.info(`Successfully created user: ${JSON.stringify(user_entity)}`);
  return user_entity_wrapper.from_user_entity_to_response(user_entity);
}
