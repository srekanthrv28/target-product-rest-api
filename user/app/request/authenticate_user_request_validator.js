/* @flow */
'use strict';

import {
  AuthenticateUserRequest,
  AuthenticateUserRequestConstraints
} from '../request/authenticate_user_request';
import * as validate from 'validate.js';
import {
  InitialAppError
} from 'tiny_error';
import R from 'ramda';


export function validate_request_async(auth_user_request: AuthenticateUserRequest): Promise<*> {
  const validation_result: mixed = validate.validate(auth_user_request, AuthenticateUserRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(auth_user_request);
  }
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}
