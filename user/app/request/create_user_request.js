/* @flow */
'use strict';

export interface CreateUserRequest {
  +email: string,
  +password: string,
  +confirm_password: string
}

export const InitialCreateUserRequest: CreateUserRequest = {
  email: '',
  password: '',
  confirm_password: ''
};

export const CreateUserRequestConstraints  = {
  email: {
    presence: true,
    email: true
  },
  password: {
    presence: true,
    length: {
      minimum: 8,
      maximum: 256,
      message: "must be at least 8 characters"
   }
  },
  confirm_password: {
    equality: "password"
  }
}
