/* @flow */
'use strict';

export interface FindUserRequest {
  +user_id: string
}

export const InitialFindUserRequest: FindUserRequest = {
  user_id: ''
}

export const FindUserRequestConstraints  = {
  user_id: {
    presence: true,
    length: {
      min: 5,
      max: 512
    }
  }
}
