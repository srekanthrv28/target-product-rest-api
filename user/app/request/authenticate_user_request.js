/* @flow */
'use strict';

export interface AuthenticateUserRequest {
  +email: string,
  +password: string
}

export const InitialAuthenticateUserRequest: AuthenticateUserRequest = {
  email: '',
  password: ''
};

export const AuthenticateUserRequestConstraints  = {
  email: {
    presence: true
  },
  password: {
    presence: true
  }
}
