/* @flow */
'use strict';

import {CreateUserRequest, CreateUserRequestConstraints, CreateUserRequestConstraintsWithOutMobile} from '../request/create_user_request';
import {FindUserRequest, FindUserRequestConstraints} from '../request/find_user_request';
import * as validate from 'validate.js';
import {InitialAppError} from 'tiny_error';
import R from 'ramda';

export function validate_create_user_request_async(create_user_request: CreateUserRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_user_request, CreateUserRequestConstraints, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_user_request);
  }
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}


export function validate_create_user_request_with_out_mobile_async(create_user_request: CreateUserRequest): Promise<*> {
  const validation_result: mixed = validate.validate(create_user_request, CreateUserRequestConstraintsWithOutMobile, {format: "grouped"});
  if (R.isNil(validation_result)) {
    return Promise.resolve(create_user_request);
  }
  return Promise.reject(R.merge(InitialAppError, {reason: "Invalid Request", type: "INVALID_REQUEST", fields: validation_result}));
}



export function validate_find_user_request_async(find_user_request: FindUserRequest): Promise<*> {
  return validate.async(find_user_request, FindUserRequestConstraints, {format: "grouped"});
}
