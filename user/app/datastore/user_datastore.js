/* @flow */
'use strict';

import {UserEntity} from '../entity/user_entity';
import * as in_memory_datastore from './in_memory_user_datastore';
import {type UserDatastoreProvider} from './user_datastore_provider';
import R from 'ramda';

const default_datastore: UserDatastoreProvider = in_memory_datastore;

export function create_datastore_user(datastore: UserDatastoreProvider, user_entity: UserEntity): Promise<*> {
  return get_datastore(datastore).create_datastore_user(user_entity);
}

export function create_new_datastore_user_without_password(datastore: UserDatastoreProvider, user_entity: UserEntity): Promise<*> {
  return get_datastore(datastore).create_new_datastore_user_without_password(user_entity);
}

export function find_datastore_user_by_email(datastore: UserDatastoreProvider, user_entity: UserEntity): Promise<*> {
  return get_datastore(datastore).find_datastore_user_by_email(user_entity);
}

export function find_datastore_user_by_user_id(datastore: UserDatastoreProvider, user_entity: UserEntity): Promise<*> {
  return get_datastore(datastore).find_datastore_user_by_user_id(user_entity);
}

export function update_datastore_user(datastore: UserDatastoreProvider, user_entity: UserEntity): Promise<*> {
  return get_datastore(datastore).update_datastore_user(user_entity);
}

function get_datastore(provided_datastore: UserDatastoreProvider): UserDatastoreProvider {
  return R.cond([
    [R.isNil, R.always(default_datastore)],
    [R.isEmpty, R.always(default_datastore)],
    [R.T, R.always(provided_datastore)]
  ])(provided_datastore);
}
