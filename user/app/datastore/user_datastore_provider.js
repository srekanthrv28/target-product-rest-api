/* @flow */
'use strict';

import {UserEntity} from '../entity/user_entity';

export interface UserDatastoreProvider {
  create_datastore_user(user_entity: UserEntity): Promise<*>,
  find_datastore_user_by_email(user_entity: UserEntity): Promise<*>,
  find_datastore_user_by_user_id(user_entity: UserEntity): Promise<*>
}
