/* @flow */
'use strict';

import {UserEntity} from '../entity/user_entity';
import datastore from 'memory-cache';
import uuid_v4 from 'uuid/v4';
import R from 'ramda';
import {logger} from 'app_log';
import {InitialAppError} from 'tiny_error';
import bcrypt from 'bcrypt';

export function create_datastore_user(user_entity: UserEntity): Promise<UserEntity | Error> {
  return R.ifElse(R.curry(does_user_already_exist), error_user_already_exist, create_new_datastore_user)(user_entity);
}

function does_user_already_exist(user_entity: UserEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(user_entity.email);
}

function does_user_by_id_already_exist(user_entity: UserEntity): boolean {
  return R.ifElse(R.curry(datastore.get), R.T, R.F)(user_entity.id);
}

export function create_new_datastore_user_without_password(user_entity: UserEntity): Promise<UserEntity | Error> {
  const _a = logger.info("Creating new user without password ", user_entity);
  const new_user_entity: UserEntity = R.merge(user_entity, {id: uuid_v4()});
  return Promise.resolve(datastore.put(new_user_entity.id, new_user_entity));
}

function create_new_datastore_user(user_entity: UserEntity): Promise<UserEntity> {
  const _a = logger.info("Creating new user... : ", user_entity);
  const new_user_entity: UserEntity = R.merge(user_entity, {id: uuid_v4(), password: encript_password(R.prop('password', user_entity))});
  const _add_user_by_id = datastore.put(new_user_entity.id, new_user_entity);
  return Promise.resolve(datastore.put(user_entity.email, new_user_entity));
}

function error_user_already_exist(user_entity: UserEntity): Promise<Error> {
  const _a = logger.error(`User with email ${R.toString(user_entity.email)} already exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `User with email: ", ${user_entity.email} already exists`, type: "INVALID_REQUEST"}));
}

export function find_datastore_user_by_email(user_entity: UserEntity): Promise<UserEntity | Error> {
  return R.ifElse(R.curry(does_user_already_exist), get_user_data, error_user_does_not_exist)(user_entity);
}

export function find_duplicate_user_by_email(user_entity: UserEntity): Promise<UserEntity | Error> {
  return R.ifElse(R.curry(does_user_already_exist), get_user_data, error_user_does_not_exist)(user_entity);
}

export function find_datastore_user_by_user_id(user_entity: UserEntity): Promise<UserEntity | Error> {
  return R.ifElse(R.curry(does_user_by_id_already_exist), get_user_data_by_id, error_user_by_id_does_not_exist)(user_entity);
}

function get_user_data_by_id(user_entity: UserEntity): Promise<UserEntity | Error> {
  return datastore.get(user_entity.id);
}

function get_user_data(user_entity: UserEntity): Promise<UserEntity | Error> {
  return datastore.get(user_entity.email);
}

function error_user_does_not_exist(user_entity: UserEntity): Promise<Error> {
  const _a = logger.error(`User with email ${R.toString(user_entity.email)} does not exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `User with email: ", ${user_entity.email} does not exists`, type: "INVALID_REQUEST"}));
}

function error_user_by_id_does_not_exist(user_entity: UserEntity): Promise<Error> {
  const _a = logger.error(`User with id ${R.toString(user_entity.id)} does not exists.`);
  return Promise.reject(R.merge(InitialAppError, {reason: `User with email: ", ${R.prop('id', user_entity)} does not exists`, type: "INVALID_REQUEST"}));
}

export function update_datastore_user(user_entity: UserEntity): Promise<*> {
  return R.ifElse(R.curry(does_user_by_id_already_exist), update_user, error_user_does_not_exist)(user_entity);
}

export function update_datastore_user_status(user_entity: UserEntity): Promise<*> {
  return R.ifElse(R.curry(does_user_by_id_already_exist), update_user, error_user_does_not_exist)(user_entity);
}


function update_user(user_entity: UserEntity): Promise<*> {
  const existing_user = datastore.get(user_entity.id);
  const updated_user = R.merge(existing_user, user_entity);
  const _update_email = datastore.put(user_entity.email, updated_user);
  return Promise.resolve(datastore.put(user_entity.id, updated_user));
}


function encript_password(password: string): string {
  return R.cond([
    [R.isNil, R.always(null)], //eslint-disable-line fp/no-nil
    [R.isEmpty, R.always(null)], //eslint-disable-line fp/no-nil
    [R.T, (value: string): string => R.curry(get_password_hash)(value)],
  ])(password);
}

function get_password_hash(password: string): string {
  return bcrypt.hashSync(password, 10);
}
